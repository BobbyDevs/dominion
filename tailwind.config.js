module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        primary:"#80008074",
        secondary:"#0B060B",
        secondary2:"#800080",
        txtColor:"#0B060B",
        txtColorLight:"#222B45",
        txtColor2:"#370352",
        txtColor3:"#2E192E",
        nav:"#140F14",
        navTwo:"#2F0847",
        hover:"#8d538dc9",
        borderColor:"#C4C4C4"
        
      },
      height: {
        '8v':"80vh",
        '9v':"90vh"
       },
       spacing: {
        //  '82':"350",
        '8p':"800px",
        '10p':"1000px",
        '9p':"900px",
        '9p5':"950px",
        '6p':"600px",
        '5p':"500px",
        '4p':"400px",
        '4p5':"450px",
        '3p':"300px",
        '3p5':"350px",
        '1p':"100px",
      }
    },

  },
  variants: {
    extend: {},
  },
  plugins: [],
}
