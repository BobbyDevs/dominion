import React, { useContext } from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "./styles/global";
import { lightTheme } from "./styles/theme";
// import { useThemeContext } from './context/themeContext'
// import LandingPage from "./pages/LandingPage";
import { userContext } from "./context/userContext";
import LandingPage from "./pages/landingPage";
import AboutPage from "./pages/About";
import { Switch, Route } from "react-router-dom";
import Believe from "./pages/Believe";
import Story from "./pages/Story";
import Team from "./pages/Team";
import Councel from "./pages/Councel";
import PrayerRoom from "./pages/PrayerRoom";
import Request from "./pages/Request";
import Share from "./pages/Share";
import Contact from "./pages/Contact";
import Sermon from "./pages/Sermon";
import SermonLiist from "./pages/SermonList";
import Navbar from "./components/Navbar";
import Footer from "./components/common/footer";

function App() {
  const { User } = useContext(userContext);

  return (
    <ThemeProvider theme={lightTheme}>
      <GlobalStyles />

      <div className="pages_container w-screen  relative">
        <Navbar/>
        <Switch>
          <Route path="/" exact>
            <LandingPage />
          </Route>

          <Route path="/about" exact>
            <AboutPage />
          </Route>

          <Route path="/believe" exact>
            <Believe />
          </Route>

          <Route path="/story" exact>
            <Story />
          </Route>

          <Route path="/team" exact>
            <Team />
          </Route>

          <Route path="/councel" exact>
            <Councel />
          </Route>

          <Route path="/prayer" exact>
            <PrayerRoom />
          </Route>

          <Route path="/request" exact>
            <Request />
          </Route>

          <Route path="/contact" exact>
            <Contact />
          </Route>

          <Route path="/share" exact>
            <Share />
          </Route>

          <Route path="/sermon" exact>
            <Sermon />
          </Route>

          <Route path="/sermon-list" exact>
            <SermonLiist />
          </Route>
        </Switch>
        <Footer/>
      </div>
    </ThemeProvider>
  );
}
export default App;
