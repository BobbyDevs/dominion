import React, { useReducer } from "react";

export const userContext = React.createContext();

const userState = {
  name: "",
  email: "",
  password: "",
};

const UserReducer = (user, action) => {
  switch (action.type) {
    case "LOG_IN":
      return {
          ...user,
          email:action.payload.email
      };
    case "LOG_OUT":
      return {
        ...user,
        email:""
      };

    default:
      return user;
  }
};

const UserContextProvider = ({ children }) => {


  const [User, dispatch] = useReducer(UserReducer, userState);

  return (
    <userContext.Provider
      value={{
          User,
          dispatch
      }}
    >
      {children}
    </userContext.Provider>
  );
};

export default UserContextProvider;
