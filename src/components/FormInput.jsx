import React from "react";
import styled from "styled-components";

const FormInput = ({
  read,
  phone,
  label,
  type,
  name,
  register,
  icon,
  req,
  placeholder,
}) => {
  return (
    <Container className="my-4 text-txtColor">
      <label htmlFor={name}>{label} *</label>
      <br />
      {true ? (
        <input className="" type="text" />
      ) : (
        <input
          id={name}
          type={type}
          {...register(name, { required: req ? true : false })}
          placeholder={placeholder}
          readOnly={read}
        />
      )}
    </Container>
  );
};

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 83px;
  label {
    color: #0b0b0b;
    font-weight: 600;
    text-transform: capitalize;
  }

  input {
    height: 48px;
    margin-top: 8px;
    width: 100%;
    background: #eddff6;
    border-bottom: 2px solid #8839b9;

    &:focus {
      outline: 1px solid transparent;
    }
  }

  .b-l {
  }
`;
export default FormInput;
