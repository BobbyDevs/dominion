import React, { useEffect } from "react";
import { NavLink, useLocation } from "react-router-dom";
// import { userContext } from "../context/userContext";
import { Nav } from "./styles";
import logo from "../../assets/logo.png";
import filter from "../../assets/filter.png";
import { RiArrowDownSLine } from "react-icons/ri";

const Navbar = ({ toggleUser }) => {
  //   const { dispatch } = useContext(userContext);

  const { pathname } = useLocation();
  useEffect(() => {
    const MobileNav = document.getElementById("MobileNav");
    MobileNav.classList.remove('active')
   
  }, [pathname])

  function toggleNavbar() {
    const MobileNav = document.getElementById("MobileNav");
    MobileNav.classList.toggle("active");
  }

  function toggleDropDown() {
    const ListOne = document.getElementById("dbl_1");
    ListOne.classList.toggle("active");
  }

  function toggleSecondDropDown() {
    const ListTwo = document.getElementById("dbl_2");
    ListTwo.classList.toggle("active");
  }

  function desktopDropdownOne() {
    const DesktopListOne = document.getElementById("drop-down-desktop-one");
    DesktopListOne.classList.toggle("active");
  }

  function desktopDropdownTwo() {
    const DesktopListTwo = document.getElementById("drop-down-desktop-two");
    DesktopListTwo.classList.toggle("active");
  }



  return (
    // <Nav className=" p-2 flex justify-between items-center  w-full h-20">
    <Nav
      className={`${
        pathname === "/"
          ? "bg-gradient-to-r from-transparent to-nav"
          : "bg-black"
      } p-2 flex justify-between items-center  w-full h-20`}
    >
      <NavLink to="/">
        <img src={logo} alt="" className="w-14 md:w-20 sm:ml-8 md:ml-12" />
      </NavLink>

      <img src={filter} alt="" className="md:hidden" onClick={toggleNavbar} />

      <ul className="navbar md:w-8p lg:w-9p  h-full hidden md:flex mr-12">
        <li
          onClick={desktopDropdownOne}
          className="nav-item  h-full  flex justify-center items-center w-48 ml-8 relative"
        >
          <button className="drop-down-btn uppercase flex justify-center items-center">
            About Us
            <RiArrowDownSLine className="ml-4" />
          </button>

          <div
            id="drop-down-desktop-one"
            className="drop_down_list w-48 absolute  top-16 bg-white rounded-b-xl p-3"
          >
            <ul className="text-black text-lg">
              <li className="my-1">
                <NavLink to="/about">About Us</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/story">Our Story</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="team">Our Team</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/believe">What We believe</NavLink>
              </li>
            </ul>
          </div>
        </li>
        <li
          onClick={desktopDropdownTwo}
          className="nav-item  h-full  flex justify-center items-center w-48 relative"
        >
          <button className="drop-down-btn uppercase flex justify-center items-center">
            Connect with us
            <RiArrowDownSLine className="ml-4" />
          </button>

          <div
            id="drop-down-desktop-two"
            className="drop_down_list w-56  absolute  top-16 left-0 bg-white rounded-b-xl p-3 "
          >
            <ul className="text-black text-lg">
              <li className="my-1">
                <NavLink to="councel">Christian Counseling</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/prayer">Prayer Room</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/request">Prayer Requests</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/share">Share Your Testimony</NavLink>
              </li>
            </ul>
          </div>
        </li>
        <li className="nav-item  h-full uppercase flex justify-center items-center w-40">
          <NavLink className="" to="sermon">
            sermons
          </NavLink>
        </li>
        <li className="nav-item  h-full uppercase flex justify-center items-center w-40">
          <NavLink className="" to="/sermon-list">
            Events
          </NavLink>
        </li>
        <li className="nav-item  h-full uppercase flex justify-center items-center w-40">
          <NavLink className="" to="contact">
            contact us
          </NavLink>
        </li>
      </ul>

      {/* mobile view modal */}
      <div
        id="MobileNav"
        className="mobile_modal text-navTwo  md:hidden fixed bg-white top-20 right-0 h-screen"
      >
        <ul className="mt-3">
          {/* drop-down-btn */}
          <li className="nav-item  h-full  flex   items-center w-full">
            <button
              onClick={toggleDropDown}
              className=" drop-down-btn font-semibold w-full uppercase  flex justify-between items-center"
            >
              About Us
              <RiArrowDownSLine className="ml-4" />
            </button>
          </li>
          {/* dropdown-item-div */}
          <div id="dbl_1" className="drop_down_list">
            <ul className="text-sm text-txtColorLight">
              <li className="my-1">
                <NavLink to="/about">About Us</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/story">Our Story</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/team">Our Team</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="believe">What We believe</NavLink>
              </li>
            </ul>
          </div>
          {/* dropdown-item-div */}

          {/* drop-down-btn */}
          <li className="nav-item  h-full  flex mt-2 items-center w-full ">
            <button
              onClick={toggleSecondDropDown}
              className="drop-down-btn font-semibold w-full uppercase flex justify-between items-center"
            >
              Connect with us
              <RiArrowDownSLine className="ml-4" />
            </button>
          </li>
          {/* dropdown-item-div */}

          <div id="dbl_2" className="drop_down_list ">
            <ul className="text-sm text-txtColorLight">
              <li className="my-1">
                <NavLink to="/councel">Christian Counseling</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/prayer">Prayer Room</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/request">Prayer Requests</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/share">Share Your Testimony</NavLink>
              </li>
            </ul>
          </div>

          {/* dropdown-item-div */}

          <li className="nav-item  h-full font-semibold my-3 uppercase flex  items-center w-full">
            <NavLink className="" to="/sermon">
              sermons
            </NavLink>
          </li>

          <li className="nav-item  h-full font-semibold my-3 uppercase flex  items-center w-full">
            <NavLink className="" to="!#">
              Events
            </NavLink>
          </li>

          <li className="nav-item  h-full font-semibold my-3 uppercase flex  items-center w-full">
            <NavLink className="" to="/contact">
              contact us
            </NavLink>
          </li>
        </ul>
      </div>

      {/* mobile view modal */}
    </Nav>
  );
};
export default Navbar;
