import styled from "styled-components";


export const Nav = styled.div`
position: fixed;
top:0;
z-index:12;
ul.navbar{
  
  div#drop-down-desktop-one{
    height:0;
  transition: all 0.4s ease;
  overflow: hidden;
  visibility: hidden;
  opacity: 0;



  &.active{
      height:160px;
  visibility: visible;
  opacity: 1;


    }


  }

  div#drop-down-desktop-two{
    height:0px;
    opacity: 0;

  transition: all 0.4s ease;
  overflow: hidden;


  &.active{
    height:160px;
  visibility: visible;
  opacity: 1;
    }

  }
}


.mobile_modal{
  width:0;
  /* padding:12px; */
  transition: all 0.4s ease;



  ul{
    width:100%;
    /* border:1px solid red; */
    li{
      padding: 5px 12px;
      &:hover{
        background: #2f08472a;
      }
    }
  }

  &.active{
    width:100%;
  }

  div#dbl_1{
    overflow: hidden;
    height: 0;
    transition: all 0.4s ease;

    &.active{
      height:150px;
    }

  }

  div#dbl_2{
    overflow: hidden;
    height: 0;
    transition: all 0.4s ease;

    &.active{
      height:150px;

    }
  }

}

`;