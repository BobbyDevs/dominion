import React from "react";
import styled from "styled-components";

const Button = ({ rad, type, text, click, width, height, mT, mR, active, sm_w }) => {
  return (
    <Container rad={rad} h={height} mT={mT} mR={mR} type={type} onClick={click} w={width} active={active} sm_w={sm_w}>
      {text}
    </Container>
  );
};
const Container = styled.button`
  width: ${({ w }) => (w ? w : "100%")};
  /* height: 52px; */
  height: ${({ h }) => (h ? h : "52px")};
  padding: 0.5rem 1rem;
  margin-top: ${({ mT }) => (mT ? mT : "0")};
  margin-right: ${({ mR }) => (mR ? mR : "0")};
  cursor: pointer;
  /* background:${({ theme, active }) => active ? theme.secondary : "none"} ; */
  color:${({ active }) => active ? "#fff" : "#fff"} ;
  /* color: #fff; */
  /* border: 1px solid red; */
  font-weight: 400;
  font-size: 14px;
  text-transform: capitalize;
  border-radius: ${({ rad }) => (rad ? rad : "30px")};
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(to right,#800080, #7312AF);

  @media screen and (max-width: 640px){
    width: ${({ w }) => (w ? `${parseInt(w) * 0.8}px` : "100%")};
    height: ${({ h }) => (h ? `${parseInt(h) * 0.8}px` : `${parseInt('52') * 0.8}px`)};
    /* width:100px;
    width: ${({ sm_w }) => (sm_w ? sm_w : "100%")};
  height: ${({ sm_h }) => (sm_h ? sm_h : "52px")}; */


  }

  /* @media screen and (min-width: 768px){
    width: ${({ width }) => (width ? width : "100%")};
  height: ${({ height }) => (height ? height : "52px")};
  }
   */

`;

export default Button;
