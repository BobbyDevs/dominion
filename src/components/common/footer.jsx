import React from "react";
import styled from "styled-components";
import insta from "../../assets/insta.png";
import face from "../../assets/face.png";
import twit from "../../assets/twit.png";
import logo from "../../assets/logo.png";
import { MdLocationOn } from "react-icons/md";
import { FaPhoneAlt } from "react-icons/fa";
import { NavLink } from "react-router-dom";

const Footer = () => {
  return (
    <Container>
      <div className="flex flex-col md:flex-row w-full md:h-40">
        
        <div className="logo_cont items-center   flex flex-col ">
          <img src={logo} alt="" className="w-28" />
          <p className="font-bold">Dominion Christian Centre</p>
        </div>

        <div className="not_logo  mt-8 md:mt-0 grid grid-cols-2 w-full md:grid-cols-4 gap-6 md:gap-3">
          
          <div className="contact font-semibold  m-2  flex flex-col items-center ">
            <span className="item flex">
              <MdLocationOn className="w-11" />
              <p className="ml-3">
                Canning Town Library, 18 Rathbone Market, London E16 1EH.
              </p>
            </span>

            <span className="item  w-full flex mt-3">
              <FaPhoneAlt />
              <a className="ml-3" href="tel:(505) 555-0125">
                (505) 555-0125
              </a>
            </span>
          </div>

          <div className="about justify-center   flex ">
            <ul className="text-black text-lg">
              <li className="my-1 font-bold">
                <NavLink to="/about">About Us</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/story">Our Story</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/team">Our Team</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/believe">What We believe</NavLink>
              </li>
            </ul>
          </div>

          <div className="connect   flex items-center">
            <ul className="text-black text-lg">
              <li className="my-1 font-bold">
                <NavLink to="/connect">Connect With Us</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/councel">Christian Counseling</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="prayer">Prayer Room</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/request">Prayer Requests</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/share">Share Your Testimony</NavLink>
              </li>
            </ul>
          </div>

          <div className="event">
            <ul className="text-black text-lg font-bold">
              <li className="my-1">
                <NavLink to="/sermon-list">Events</NavLink>
              </li>

              <li className="my-1">
                <NavLink to="/contact">Contact Us</NavLink>
              </li>

              <li className="my-1 flex justify-between w-24">
                <NavLink to="!#">
                  <img src={face} alt="" />
                </NavLink>
                <NavLink className="text-secondary2" to="!#">
                  <img src={twit} alt="" />
                </NavLink>
                <NavLink className="" to="!#">
                  <img src={insta} alt="" />
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Container>
  );
};

const Container = styled.section`
  width: 100%;
  height: 100%;

  background: #fff;
  padding: 5rem 4rem;
  position: relative;
  font-family: "Montserrat", sans-serif;
  color: #0b060b;

  /*  */
  @media screen and (max-width: 600px) {
    padding:1.5rem;
  }
`;

export default Footer;
