import React from "react";
import styled from "styled-components";


// export const 


const InputField = ({ type, name, register, icon, req, placeholder }) => {
	return (
		<Container>
			<div className='icon_container'>
				{icon === "user" ? (
					<span
						class='iconify'
						data-icon='bx:bxs-user'
						data-inline='false'></span>
				) : (
					<span
						class='iconify' 
						data-icon='jam:padlock-f'
						data-inline='false'></span>
				)}
			</div>

			<input
				type={type}
				{...register(name, { required: req ? true : false })}
				placeholder={placeholder}
			/>
		</Container>
	);
};

const Container = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	border: 1px solid #ebddf2;
	border-radius: 5px;
	background-color: #fff;
	/* box-shadow:
	#EBDDF2 0px 1px 3px 1px; */
	width: 100%;
	height: 42px;
	margin: 0.5rem 0;

	.icon_container {
		width: 12%;
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;

		.iconify {
			font-size: 19px;
			color: #391b4a;
			margin-left:8px;
		}
	}

	input {
		width: 90%;
		height: 100%;
		padding: 1rem;
		font-size: 15px;
		border: none;

		&:focus {
			outline: none;
		}
	}
`;
export default InputField;
