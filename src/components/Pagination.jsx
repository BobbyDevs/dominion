import React from "react";
import { BsArrowLeft, BsArrowRight } from "react-icons/bs";
import styled from "styled-components";

const Pagination = ({
  items,
  itemPerPage,
  paginate,
  currentPage,
  setcurrentPage,

}) => {
  const pageNumbers = [];

  for (let i = 1; i < Math.ceil(items.length / itemPerPage); i++) {
    pageNumbers.push(i);
  }

 console.log(pageNumbers.length, currentPage  );
  return (
    <>
      <Paginations className="pagination w-full flex h-12 bg-white justify-between">
        <Btn
          onClick={() => setcurrentPage(currentPage - 1)}
          className="prev ml-3 bg-red-white centralize w-24"
          disabled={currentPage === pageNumbers.length - (pageNumbers.length - 1) ? true : false }
        >
          {" "}
          <BsArrowLeft style={{ marginRight: "1rem" }} />
          Prev
        </Btn>
        <div className="p_count  flex">
          {/* <li className="pages w-8 centralize  ">1</li> */}
          {pageNumbers.map((item, index) => (
            <li
              id={index}
              onClick={() => paginate(item, index)}
              className={
                currentPage === item
                  ? "pages w-8 centralize text-txtColor active"
                  : "pages w-8 centralize text-txtColor "
              }
            >
              {item}
            </li>
          ))}
          {/* <li className="pages w-8 centralize  ">3</li> */}
        </div>
        <Btn
          disabled={currentPage === pageNumbers.length ? true : false }
          // disabled={false}
          onClick={() => setcurrentPage(currentPage + 1)}
          className="next mr-3 centralize  w-24"
        >
          Next <BsArrowRight style={{ marginLeft: "1rem" }} />
        </Btn>
      </Paginations>
    </>
  );
};

const Paginations = styled.ul`
  border-radius: 0 0 8px 8px;

  li {
    border: 2px solid transparent;
    color: #8c58a7;
    position: relative;

    &.active {
      /* border-bottom-color:#52057B; */
      &:after {
        content: "";
        position: absolute;
        bottom: -2.5px;
        width: 100%;
        height: 2px;
        background-color: #52057b;
      }
    }
  }
`;
const Btn = styled.button`


  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;
export default Pagination;
