import React from "react";

import styled from "styled-components";
import hero from "../assets/sermon.png";

import img3 from "../assets/mount.png";
import img4 from "../assets/you_icon.png";

import { useForm } from "react-hook-form";




const SermonCard = ({ id }) => {
  return (
    <div className="sermon-cards  w-full border-t-2  h-3p ">
        <div className="image_container rounded-lg relative h-full w-full ">
            <img src={img3} alt="" className="h-full w-full " />
          
            <div className="image_cover w-full h-32 absolute top-0 p-3">
              <div className=" content">
                <p className="capitalize date text-white">jan 24, 2020</p>
                <p className="text-white text-xl">
                  Gather: Build Authentic
                  <br />
                  Relationships with God
                </p>
                <p className="capitalize date text-primary">Johnson Meyer</p>
              </div>
            </div>
          </div>
        </div>
  );
};


const Sermon = () => {
  const { register } = useForm();

  return (
    <Hero>
      <div className="hero w-full h-screen relative">
       
        <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-2xl md:text-6xl about_us">sermons</h1>
        </div>
      </div>

      {/*  */}
      <Sermons className="">
      <h1 className="title  font-bold p-2 md:mx-auto relative text-left md:text-center w-52    text-2xl text-txtColor capitalize">
          Latest Sermons
        </h1>


        <div className="sermon-card mt-11 md:w-8p mx-auto ">
        <div className="image_container rounded-lg h-4p  relative">
            <img src={img3} alt="" className="h-4p w-full " />
            <img
              src={img4}
              alt=""
              className="absolute top-1/2 left-1/2 transform -translate-y-1/2 -translate-x-1/2"
            />
            <div className="p-2 image_cover w-full h-40 absolute bottom-0 rounded-b-xl md:p-8">
              <div className=" content">
                <p className="capitalize date text-primary">jan 24, 2020</p>
                <p className="text-white text-2xl">
                  Gather: Build Authentic
                  <br />
                  Relationships with God
                </p>
                <p className="capitalize date text-primary">Johnson Meyer</p>
              </div>
            </div>
          </div>
        </div>


        <div className="gallery grid-cols-1 grid md:grid-cols-3 w-full border gap-8 mt-16">
    
     {[1, 1, 1, 1, 1,1,1,1, 1].map((item, index) => (
        <SermonCard id={index} />
      ))}
    
        </div>
      </Sermons>
      {/*  */}

     
    </Hero>
  );
};

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    background-repeat: no-repeat;
    height: 90vh;

    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const Sermons = styled.section`
  width: 100%;
  /* height: 100vh; */
  background: #F9F9FA;
  padding: 8rem 7rem;
  position: relative;
  /* border:1px solid red; */

  @media screen and (max-width: 600px) {
  padding: 5rem 1.5rem;
    }


  .image_cover {
    background: #3a363d;
    font-family: "Merriweather", serif;
    .date {
      color: #c883f2;
    }
  }

  .sermon-cards{
    border-color:#880ED4;
  }


  .list_item{
      border-bottom:2px solid #8D538D;
  }

 
  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 50px;

      /* transform: translateX(-50%); */
    }}
`;

export default Sermon;
