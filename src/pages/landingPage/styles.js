import styled from "styled-components";
import hero from "../../assets/hero_1.webp";

export const Hero = styled.div`
  @media screen and (max-width: 600px) {
    height: 100%;
  }

  .hero {
    background-image: url(${hero});
background-repeat:no-repeat;
    border: 1px solid black;
    height: 100vh;

    @media screen and (max-width: 600px) {
      padding: 1.5rem;
      height: auto;
background-size:auto 100%;
border: none;


    }

    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      /* width: 990px; */

      @media screen and (max-width: 470px) {
        margin-left: 0rem;
        width: 300px;
      }
    }

    .join_us {
      .card {
      }
    }
  }
`;

export const About = styled.section`
  width: 100%;
  height: 100vh;
  background: #f9f6fb;
  padding: 5rem 1.4rem;
  position: relative;
  @media screen and (max-width: 600px) {
      height:115vh;
    }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 29px;

      /* transform: translateX(-50%); */
    }
  }

  .learn{
    box-shadow: 0 2.89px 5.45px 0 rgba(45, 13, 140, 0.179);
    color:#850FD0;
  }
`;

export const Sermons = styled.section`
  width: 100%;
  height: 100vh;
  background: #fff;
  padding: 5rem 7rem;
  position: relative;

  @media screen and (max-width: 600px) {
      height:150vh;
  padding: 5rem 1.5rem;

    }

  .image_cover {
    background: #3a363d;
    font-family: "Merriweather", serif;
    .date {
      color: #c883f2;
    }
  }

  .list_item {
    border-bottom: 2px solid #8d538d;
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 29px;

      /* transform: translateX(-50%); */
    }
  }
`;

export const Meet = styled.section`
  width: 100%;
  height: 100vh;
  background: #f9f6fb;
  padding: 5rem 10rem;
  position: relative;
  font-family: "Montserrat", sans-serif;

  @media screen and (max-width: 600px) {
      
  padding: 5rem 1.5rem;

    }

    .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 50px;

      /* transform: translateX(-50%); */
    }
  }
`;
