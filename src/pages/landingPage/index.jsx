import React from "react";
import { Hero, About, Sermons, Meet } from "./styles";
import { NavLink } from "react-router-dom";
// import Navbar from "../../components/Navbar";
import Button from "../../components/common/Button";
import { BiPlayCircle } from "react-icons/bi";
import img1 from "../../assets/img1.png";
import img2 from "../../assets/img2.png";
import img3 from "../../assets/you_thumb.png";
import img4 from "../../assets/you_icon.png";
import pastor from "../../assets/pastor.png";
import { RiArrowRightSLine } from "react-icons/ri";

const LandingPage = () => {
  return (
    <>
      <Hero>
        <div className="hero w-full ">
          {/*  */}
          <div className="hero_content  w-full  flex flex-col justify-between  md:w-6p h-64 md:h-80 text-left  md:text-center mt-16 md:mt-32   md:mx-auto">
            <h1 className="welcome text-3xl md:text-5xl mt-4 font-medium">
              Welcome to Dominion Christian Centre
            </h1>

            <p className="subtext text-md md:text-xl md:mt-6 ">
              We are a growing charismatic church based in the Docklands area of
              London, England.
            </p>

            <div className="action_container flex  md:justify-center  md:mt-6 items-center">
              <Button text="Learn more" width="178px" sm_w="130px" />
              <span className="watch capitalize flex items-center  p-3 ">
                <BiPlayCircle className="mr-1 md:mr-3" />
                <p className="learn_more text-sm ">watch a sermon</p>
              </span>
            </div>
          </div>
          {/*  */}

          {/* jooin us */}
          <div className=" join_us_container  flex flex-col md:flex-row  md:w-9p5 mt-20 md:mt-11">
            <h1 className=" text-2xl md:text-sm">join Us :</h1>

            <div className="join_us mt-6 md:mt-0  text-sm md:mx-auto gap-4 md:gap-2  h-full md:h-30 grid grid-cols-1 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
              <div className="card  bg-primary p-4 rounded-xl w-72">
                <div className="time flex justify-between font-bold">
                  <p className=" capitalize ">every Sunday</p>

                  <p>11.30am - 1pm</p>
                </div>

                <div className="location mt-4">
                  <p className="capitalize text-sm">
                    Daisy Parsons Hall, Canning Town Library, 18, Rathbone
                    Market London E16 1EH.
                  </p>
                </div>
              </div>

              <div className="card  bg-primary p-4 rounded-xl w-72">
                <div className="time flex justify-between font-bold">
                  <p className=" capitalize ">every Wednesday</p>

                  <p>9.3 0pm</p>
                </div>

                <div className="location mt-6 flex justify-between text-md">
                  <p className="capitalize">online via zoom</p>

                  <NavLink to="https://zoom.us/j/8267348219#" className="">
                    <u>Join</u>
                  </NavLink>
                </div>
              </div>

              <div className="card  bg-primary p-4 rounded-xl w-72">
                <div className="time flex justify-between font-bold">
                  <p className=" capitalize ">every Friday</p>

                  <p>10pm</p>
                </div>

                <div className="location mt-6">
                  <p className="capitalize">Temporarily online via zoom</p>
                </div>
              </div>
            </div>
          </div>
          {/* jooin us */}
        </div>
      </Hero>
      <About>
        <h1 className="title font-bold pl-0 md:pl-2 p-2  md:mx-auto relative text-left  md:text-center w-48 text-3xl text-txtColor capitalize">
          About us
        </h1>

        <div className="content w-full md:w-5p mt-8 md:mt-16 md:ml-20 text-xl">
          <p className="one text-txtColor font-normal">
            Dominion Christian Centre is a growing charismatic church based in
            the Docklands area of London, England. At DCC, we believe that God
            has ordained a future for everyone which they have to possess.
            <br />
            <br />
            At DCC, we preach Jesus Christ and lives of many are transformed...
          </p>
        </div>
        {/*  */}
        <br />
        <NavLink
          to="!#"
          className="learn w-28 h-5 relative bg-white md:ml-20 text-primary p-4 rounded-full font-bold"
        >
          Learn more
        </NavLink>

        {/*  */}

        <img
          src={img1}
          alt=""
          className="absolute md:right-52 bottom-40 md:top-40 w-3p5 md:w-96"
        />
        <img
          src={img2}
          alt=""
          className="absolute md:right-52 bottom-0 md:bottom-20 md:top-90 w-3p5 h-36 md:h-auto md:w-5p "
        />

        {/*  */}
      </About>
      {/* sermon */}
      <Sermons>
        <h1 className="title font-bold pl-0 md:pl-2 p-2  md:mx-auto relative text-left  md:text-center w-40 text-3xl text-txtColor capitalize">
          Sermons
        </h1>

        <div className="row flex flex-col md:flex-row justify-between w-full h-4p mt-12 ">
          <div className="image_container rounded-lg h-4p w-full md:w-6p relative">
            <img src={img3} alt="" className="h-4p w-full " />
            <img
              src={img4}
              alt=""
              className="absolute top-1/2 left-1/2 transform -translate-y-1/2 -translate-x-1/2"
            />
            <div className="image_cover w-full h-40 absolute bottom-0 rounded-b-xl p-8">
              <div className=" content">
                <p className="capitalize date text-primary">jan 24, 2020</p>
                <p className="text-white text-2xl">
                  Gather: Build Authentic
                  <br />
                  Relationships with God
                </p>
                <p className="capitalize date text-primary">Johnson Meyer</p>
              </div>
            </div>
          </div>
          <div className="sermon_list mt-16 md:mt-0  h-full  md:w-4p text-txtColor">
            <span className="more flex justify-between">
              <p className="font-medium text-xl">More sermons</p>

              <NavLink to="/sermon-list">
                <u className="uppercase font-bold">view all</u>
              </NavLink>
            </span>

            <div className="list-item-conainer mt-8 md:mt-4">
              {[1, 1, 1, 1].map((item) => (
                <div className="list_item p-2  flex items-center justify-between py-8 h-20 mt-3 hover:bg-hover">
                  <div className=" content">
                    <p className="capitalize date text-primary">jan 24, 2020</p>
                    <p className="text-txtColor font-medium text-xl">
                      Gather: Build Authentic
                      <br />
                      Relationships with God
                    </p>
                  </div>
                  {/*  */}
                  <RiArrowRightSLine />
                </div>
              ))}
            </div>
          </div>
        </div>
      </Sermons>
      {/* sermon */}

      {/* meet */}
      <Meet>
        <h1 className="title font-bold pl-0 md:pl-2 p-2  md:mx-auto relative text-left  md:text-center w-64  text-2xl text-txtColor capitalize">
          Meet Our Pastor
        </h1>

        <div className="row flex flex-col md:flex-row  justify-between w-full h-4p mt-12 text-txtColor3">
          <div className="image_container  rounded-lg h-4p md:w-6p ">
            <h1 className="font-bold text-3xl">Pastor Yinka Popoola</h1>

            <p className="text-sm w-full md:w-96 leading-4 my-3 font-medium">
              Yinka Popoola, a qualified Building/CAD Engineer, is the Senior
              Pastor of Dominion Christian Centre based in East London, United
              Kingdom.
              <br />
              <br />
              An anointed preacher, his inspiring messages, anointed biblical
              exposition and prophetic prayers have positively transformed the
              lives of many people.
              <br />
              <br />
              His overwhelming passion is to preach Christ and transform lives
              of people in order for them to discover and possess their
              God-ordained future.
              <br />
              <br />
              He is married to Mubo who is a faithful partner and a co-labourer
              in God’s vineyard and they have three children, David Esther and
              Grace.
            </p>
          </div>

          <div className="sermon_list h-full md:w-5p text-txtColor">
            <img src={pastor} alt="" />
          </div>
        </div>
      </Meet>
    </>
  );
};

export default LandingPage;
