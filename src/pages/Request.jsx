import React from "react";
import { useForm } from "react-hook-form";
import styled from "styled-components";
import hero from "../assets/prayer.png";
import Button from "../components/common/Button";
import FormInput from "../components/FormInput";

const Request = () => {
  const { register } = useForm();
  return (
    <Hero>
      <div className="hero w-full h-screen relative">
     
        <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-4xl md:text-6xl about_us">prayer rquest </h1>
        </div>
      </div>

      {/*  */}
      <AboutUs className="about_us_text">
        <p className="one text-txtColor md:ml-40 md:mt-6 font-normal md:w-8p text-left">
          We believe that God not only hears us, but that He also responds to us
          in various ways. Whether it’s by an illness healed, a relationship
          restored or an awareness of his presence … God responds to His people.
          <br />
          <br />
          Submit a prayer request below:
        </p>

        {/*  */}
        <form className=" md:w-6p mx-auto h-auto mt-8">
          <div className=" grid grid-cols-1" gap-6>
            <div className="sub-form-container gap-4 grid grid-cols-1 md:grid-cols-2">
              <FormInput
                placeholder="firstname"
                name="name"
                type="tex"
                label="First Name"
                register={register}
              />
              <FormInput
                placeholder="lastname"
                name="name"
                label="Last Name"
                register={register}
              />
            </div>
            <FormInput
              placeholder="email"
              name="name"
              type="tex"
              label="Email Address"
              register={register}
            />
            <FormInput
              placeholder="phone"
              name="name"
              label="Phone Number"
              register={register}
            />
            <FormInput
              placeholder="prayer"
              name="name"
              label="prayer"
              register={register}
            />
          </div>

          <div className="radio_btn  text-txtColor">
            <p className="text">
              Would you like to be contacted by a Pastor? *
            </p>
            <div className="flex items-center mt-3">

            <input type="radio" name="yes" id="yes" value="Yes" />
            <label className="ml-4" htmlFor="yes">Yes</label>
            <input className="ml-4"  type="radio" name="no" id="No" value="No" />
            <label className="ml-4" htmlFor="yes">No</label>
            </div>
          </div>

          <Button mT="0.75rem" text="submit" type="submit" width="164px" />
        </form>
      </AboutUs>
      {/*  */}
      <div />

    </Hero>
  );
};

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    background-repeat: no-repeat;
    height: 50vh;

    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const AboutUs = styled.section`
  width: 100%;
  background: #f9f9fa;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;

  @media screen and (max-width: 600px) {
  padding: 5rem 1.5rem;
    }


  .form-card{
    box-shadow: 0 5px 15px  0 #880ed43c;
    border-radius:8px;
  }
  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;
  }
`;

export const Info = styled.section`
  width: 100%;
  height: 500px;
  background: #f7ebff;
  display: flex;
  align-items: center;
  padding: 5rem 10rem;
  font-family: "Montserrat", sans-serif;
`;
export default Request;
