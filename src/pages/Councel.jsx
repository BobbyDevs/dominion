import React from "react";

import styled from "styled-components";
import hero from "../assets/councel.png";
import councelPic from "../assets/councelPic.png";

const Councel = () => {
  return (
    <Hero>
      <div className="hero w-full h-screen relative">
     
      <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-2xl md:text-6xl about_us">christian couseling</h1>
        </div>
      </div>

      {/*  */}
      <AboutUs className="about_us_text">
        <p className="one text-txtColor md:ml-40 mt-6 font-normal md:w-8p text-left md:text-center text-xl">
          The counseling team at Dominion Christian Centre offers confidential,
          Christian Counseling to individuals, couples and families who are
          facing challenges in life. Our philosophy is based on empowering
          people of all ages to live life by biblical standards, first, by
          listening to their needs and then following the leading of the Holy
          Spirit.
        </p>
      </AboutUs>
      {/*  */}

      <Info>
        <div className="row flex flex-col-reverse md:flex-row justify-between w-full">
          <div className="image_container rounded-lg mt-8 md:mt-0 md:w-4p relative">
            <img src={councelPic} alt="" className="w-full" />
          </div>
          <div className="sermon_list  h-full md:w-4p text-txtColor text-sm">
            <div className="list-item-conainer  mt-4">
              <h1 className=" subtitle font-bold text-2xl md:text-3xl">Schedule an appointment</h1>
              <p className="text text-lg">
                To schedule an individuals, couples, or family counseling
                appointment with us, please call the Dominion Christian Centre
                office at <b>417-581-5433</b>. or send us a mail at <b>counseling@dcc.com</b>
                <br />
                <br />
                We can’t wait to connect with you!
              </p>
            </div>
          </div>
        </div>
      </Info>
      <div />

    
    </Hero>
  );
};

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    background-position:center;
    height: 90vh;

    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const AboutUs = styled.section`
  width: 100%;
  /* height: 40vh; */
  background: #fff;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;

  @media screen and (max-width: 600px) {
  padding: 2rem 1.5rem;
    }



    .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 69px;

      /* transform: translateX(-50%); */
    }
  }
`;

export const Info = styled.section`
  width: 100%;
  height: 500px;
  background: #f7ebff;
  display:flex;
  align-items: center;
  padding: 5rem 10rem;
  font-family: "Montserrat", sans-serif;

  @media screen and (max-width: 600px) {
  padding: 2rem 1.5rem;
  height: 100%;

    }

    .subtitle{
      font-family: "Merriweather", serif;
    }


`;
export default Councel;
