import React from "react";

import styled from "styled-components";
import hero from "../assets/prayer.png";

import Button from "../components/common/Button";

const PrayerRoom = () => {
  return (
    <Hero>
      <div className="hero w-full h-screen relative">
        <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-4xl md:text-6xl about_us">
            prayer room{" "}
          </h1>
        </div>
      </div>

      {/*  */}
      <AboutUs className="about_us_text">
        <p className="one text-txtColor md:ml-40  md:mt-6 font-normal md:w-8p text-left">
          Need Prayer? We’d love to connect with you! We believe that life was
          never meant to be lived alone. Whether it’s big or small, we want to
          partner with you in declaring the Good News of Jesus Christ into any
          and every situation!
          <br />
          <br />
          We have put together some amazing resources to make you pray without
          ceasing. Download them and be blessed.
        </p>

        <div className="content-one text-txtColor md:ml-40 mt-6 font-normal md:w-8p text-left">
          <h1 className="header font-bold">
            2020 JUNE - RESTORATION PRAYERS FOR REFERENCE
          </h1>
          <ul className="one text-txtColor list-disc ml-8 mt-8">
            <li className="item">
              Thank God for His goodness, mercy, love and provisions.
            </li>
            <li className="item">
              Confess and repent of all sins, known and unknown.
            </li>
            <li className="item">
              Touch your lips and confess this: “my mouth is anointed with the
              power of God Almighty”.
            </li>
            <li className="item">
              I decree that every word that come out of my mouth as I pray today
              must be fulfilled in Jesus’ name.
            </li>
          </ul>

          <Button
            mT="2rem"
            text="download full document"
            width="290px"
            height="50px"
          />
        </div>

        {/*  */}
        <div className="content-one text-txtColor md:ml-40 mt-6 font-normal md:w-8p text-left">
          <h1 className="header font-bold">Prophetic Prayers</h1>

          <p className="font-black text-sm">week 1</p>
          <ul className="one text-txtColor list-disc ml-8">
            <li className="item">
              {" "}
              Prayers of thanksgiving for all He has done in our lives.
            </li>
            <li className="item">
              Plead the blood of Jesus as you approach the throne of grace.
            </li>
            <li className="item">
              Ask for boldness to enter His presence – Heb. 4: 16.
            </li>
            <li className="item">
              Lord, frustrate the devices of the crafty against my life – Job
              5:12.
            </li>
          </ul>

          <Button
            mT="2rem"
            text="download full document"
            width="290px"
            height="50px"
          />
        </div>
      </AboutUs>
      {/*  */}
      <div />
    </Hero>
  );
};

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    background-repeat: no-repeat;
    height: 50vh;

    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const AboutUs = styled.section`
  width: 100%;
  background: #f9f9fa;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;

  @media screen and (max-width: 600px) {
    padding: 2rem 1.5rem;
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;
  }
`;

export const Info = styled.section`
  width: 100%;
  height: 500px;
  background: #f7ebff;
  display: flex;
  align-items: center;
  padding: 5rem 10rem;
  font-family: "Montserrat", sans-serif;
`;
export default PrayerRoom;
