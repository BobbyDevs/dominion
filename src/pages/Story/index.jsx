import React from "react";
import { Hero, Content} from "./styles";

const Story = () => {
  return (
    <Hero>
      <div className="hero w-full h-screen relative">
       
      <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-4xl md:text-6xl about_us">our story</h1>
        </div>
      </div>

      {/*  */}
      <Content className="about_us_text h-full">
        <h1 className="title  font-bold p-2 md:mx-auto relative text-left md:text-center w-full text-xl md:text-3xl text-txtColor capitalize">
          Brief History of Dominion Christian Centre
        </h1>

        <p className="one text-txtColor  md:ml-40 mt-6 font-normal md:w-8p text-left">
          DCC started in March 1999, when God called Pastor Yinka Popoola with
          three basic mandates which formed the foundation of the vision and
          mission of the church.
          <br />
          <br />
          The three mandates are: Firstly, to go and set the captives free in
          the name of Jesus (Matt. 16:19; Acts 10:38; Luke 4:18). Secondly, that
          Pastor Yinka was being sent out as a reaper on the field of life in
          Britain ( John 4:34-38) Thirdly, to revive the people of God by
          teaching them to pray prophetically using God’s word and subsequently
          revive the land by the power of the Holy Ghost. (1 Sam.3:11; Isa.
          41:18-19.)
          <br />
          <br />
          To the glory of God, the ministry has flourished with signs and
          wonders following. Lives have truly been transformed with various
          types of healings and miracles and an incise delivery of the word of
          God that keeps growin
          <br />
          <br />
          The ministry has touched so many lives both in Britain and around the
          world via systematic media outreaches. The “Walking in Dominion TV
          program” is a favourite watch and the Walking in dominion articles and
          press releases in various international magazines has produced a
          tremendous followership around the world.
        </p>

        <div className="time_line  text-txtColor  md:ml-40 mt-6 font-normal md:w-8p text-left">
          <ul className="list-disc">
            <li className="ml-4 font-black text-xl">1999</li>
            <p>
              We started from Keir Hardie Methodist Church in Canning Town,
              London E16. The church was inaugurated there and we were in
              Canning Town for 2 years. The church started with about 8 members
              and grew in this small church hall until persecution arose because
              of our growth and thereby God instructed us to move out of the
              place. This was confirmed by two invited ministers who came to
              minister to us shortly after the instruction was given.
            </p>

            <li className="ml-4 font-black text-xl mt-8">2001</li>
            <p>
              We moved to a school hall in April 2001, precisely St Mary’s
              School in Plaistow, London E13. We were there for just 8 months
              before moving out due to the closure of the school hall for
              Christmas/New Year Break.
            </p>

            <li className="ml-4 font-black text-xl mt-8">2002</li>
            <p>
              We moved in to a small shop on Hermit Road in Canning Town, London
              E16. We were there for 18 months until we grew to a point when we
              started having 2 services every Sunday.
            </p>

            <li className="ml-4 font-black text-xl mt-8">2003</li>
            <p>
              We moved in to Custom House Hotel, Victoria Dock, London E16. We
              were there for 10 months until we grew to a point when we started
              having two services every Sunday. We moved from the hotel when car
              parking became a problem. We then applied for change of use
              planning for the First and second floor of Canning Town Kwik Save
              building. This application was refused.
            </p>

            <li className="ml-4 font-black text-xl mt-8">2004</li>
            <p>
              It was during this time that the Lord spoke to Pastor Yinka
              Popoola to ‘Go to Barking’. He decided to drive through the area
              to find out if he would find a Hall space. He couldn’t find any
              Hall space. A few days in the church board meeting, someone just
              mentioned in passing that there was a Hall available to rent in
              Barking. That was how Dominion Christian Centre moved to the first
              floor, 6 Thames Road.
            </p>

            <li className="ml-4 font-black text-xl mt-8">2005</li>
            <p>
              The Church was able to secure a lease of the entire first floor.
              The church grew until we started to have two services. It was at
              this point, that the Pastor approached the landlord for the two
              warehouses on the ground floor. This proposal was turned down by
              the landlord
            </p>

            <li className="ml-4 font-black text-xl mt-8">2006/2007</li>
            <p>
              We continued to pray for wisdom and favour. One day as the Pastor
              was approaching the building, the holy spirit spoke to the pastor
              to go once more to find out if the landlord could give us more
              space on the first floor. To the pastor’s surprise the landlord
              asked whether we were still interested in the two warehouses on
              the ground floor that we asked for the previous year. The two
              warehouses were refurbished and the building was dedicated to the
              glory of God on the 7th July 2007.
            </p>
          </ul>
        </div>
      </Content>
      {/*  */}

      {/*  */}

    
    </Hero>
  );
};

export default Story;
