import styled from "styled-components";
import hero from "../../assets/story.png";

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    height:90vh;


    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const Content = styled.section`
  width: 100%;
  /* height: 40vh; */
  background: #fff;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;

  @media screen and (max-width: 600px) {
  padding: 5rem 1.5rem;
    }

  .content {
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 65px;

      /* transform: translateX(-50%); */
    }
  }
`;

export const Values = styled.section`
  width: 100%;
  height: 100vh;
  background: #FCF9FE;
  padding: 5rem 7rem;
  position: relative;


  .image_cover {
    background: #3a363d;
    font-family: "Merriweather", serif;
    .date {
      color: #c883f2;
    }
  }


  .list_item{
      border-bottom:2px solid #8D538D;
  }

 
  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;
  }
`;

export const Mandate = styled.section`
  width: 100%;
  height: 100vh;
  background: #fff;
  padding: 5rem 10rem;
  position: relative;
  font-family: 'Montserrat', sans-serif;

  .content {
  }

  .title{
      width:280px;
  }
  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 10px;
    height: 3px;
    background: red;
    /* background: #991ee4; */
  }

  ol{
    list-style: decimal;
    /* background: red; */
    font-family: 'Montserrat', sans-serif;
  }
`;
