import React from "react";
import { Hero, AboutUs} from "./styles";


const Believe = () => {
  return (
    <Hero>
      <div className="hero w-full h-screen relative">
      
        <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-4xl md:text-6xl about_us">our belief</h1>
        </div>
      </div>

      {/*  */}
      <AboutUs className="about_us_text h-screen">
        <h1 className="title  font-bold p-2 md:mx-auto relative text-left md:text-center md:w-48 text-3xl text-txtColor capitalize">
          Our Belief
        </h1>

        <p className="one text-txtColor  md:ml-40 mt-6 font-normal md:w-8p text-left">
          The Church, Dominion Christian Centre is committed to Biblical message
          for Christians and what we must believe and practice to stay in shape
          spiritually. We live in a period of history when people care little
          about what they believe. A false sign will lead to false conclusion.
          <br />
          <br />
          Like Saint Luke, we feel led of the Holy Spirit “to set forth in order
          a declaration of those things which are most surely believed among
          us”. That through its influence souls may be delivered from the power
          of darkness and become “partakers of the inheritance of the saints in
          light.”
          <br />
          <br />
          Examining what we stand for in the light of God’s word, and by the
          enlightenment of His Spirit, we may see the devices of the wicked one,
          and the danger that must be avoided if we would be accounted faultless
          by the Lord at His coming.
          <br />
          <br />
          “That the generation to come might know them even the children which
          should be born; who should arise and declare them to their children;
          That they might set their hope in God, and not forget the works of
          God, but keep His commandments”.
        </p>
      </AboutUs>
      {/*  */}

  
    </Hero>
  );
};

export default Believe;
