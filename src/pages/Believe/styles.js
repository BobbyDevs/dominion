import styled from "styled-components";
import hero from "../../assets/hero_1.webp";

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    height:90vh;


    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const AboutUs = styled.section`
  width: 100%;
  height: 100%;
  background: #fff;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;

  @media screen and (max-width: 600px) {
  padding: 5rem 1.5rem;
    }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 69px;

      /* transform: translateX(-50%); */
    }
  }
`;

