import React from "react";

import styled from "styled-components";
import hero from "../assets/tele.png";
import Button from "../components/common/Button";
import FormInput from "../components/FormInput";
import { useForm } from "react-hook-form";

const Contact = () => {
  const { register } = useForm();

  return (
    <Hero>
      <div className="hero w-full h-screen relative">
       
      <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-2xl md:text-6xl about_us">contact us</h1>
        </div>
      </div>

      {/*  */}
      <Testimony className="">
        <div className="flex flex-col md:flex-row justify-between md:h-6p items-center md:w-10p mx-auto ">
          {/*  */}
          <div className="info flex justify-between md:w-5p flex-col h-80  md:h-full">
            <p className="font-bold">
              We desire to connect you with our community and congregation, so
              please feel free to reach out to us with any questions, thoughts,
              needs, or prayer requests. A staff member will reach back as soon
              as possible to connect.
            </p>

            <div className=" border-t-2 border-borderColor">
              <p className="font-bold mt-2 text-sm">Share your Story via a Call</p>
              <h1 className="text-2xl mt-4 font-black">(308) 555-0121</h1>
            </div>
          </div>
          {/*  */}

          <form className="form-card  mt-16 md:mt-0 w-full md:w-4p p-4 md:p-8">
          <FormInput
                placeholder="firstname"
                name="name"
                type="tex"
                label="First Name"
                register={register}
              />
              <FormInput
                placeholder="lastname"
                name="name"
                label="Last Name"
                register={register}
              />
        
            <FormInput
              placeholder="email"
              name="name"
              type="tex"
              label="Email Address"
              register={register}
            />
            <FormInput
              placeholder="phone"
              name="name"
              label="Phone Number"
              register={register}
            />
            <FormInput
              placeholder="prayer"
              name="name"
              label="prayer"
              register={register}
            />
            <Button mT="0.75rem" text="submit" type="submit" width="164px" />
          </form>
        </div>
      </Testimony>
      {/*  */}


    </Hero>
  );
};

export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    background-repeat: no-repeat;
    height: 90vh;

    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;

export const Testimony = styled.section`
  width: 100%;
  background: #f9f9fa;
  height: 100%;
  
  /* background: #f9f6fb; */
  padding:10rem 5rem;
  position: relative;
  color: #0b060b;

 
  @media screen and (max-width: 600px) {
  padding: 2rem 1.5rem;
    }

  .form-card{
    box-shadow: 0 5px 15px  0 #880ed43c;
    border-radius:8px;
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 65px;

      /* transform: translateX(-50%); */
    }
  }
`;

export const Info = styled.section`
  width: 100%;
  height: 500px;
  background: #f7ebff;
  display: flex;
  align-items: center;
  padding: 5rem 10rem;
  font-family: "Montserrat", sans-serif;
`;
export default Contact;
