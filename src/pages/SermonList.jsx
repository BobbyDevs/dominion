import React from "react";
import Navbar from "../components/Navbar";

import styled from "styled-components";
import hero from "../assets/sermon.png";
import Footer from "../components/common/footer";

import main from "../assets/sermon_frame.png";
import img3 from "../assets/mount.png";


import { useForm } from "react-hook-form";
import Button from "../components/common/Button";

const SermonCard = ({ id }) => {
  return (
    <div className="sermon-cards  w-full border-t-2  h-3p ">
      <div className="image_container rounded-lg relative h-full w-full ">
        <img src={img3} alt="" className="h-full w-full " />

        <div className="image_cover w-full h-32 absolute top-0 p-3">
          <div className=" content">
            <p className="capitalize date text-white">jan 24, 2020</p>
            <p className="text-white text-xl">
              Gather: Build Authentic
              <br />
              Relationships with God
            </p>
            <p className="capitalize date text-primary">Johnson Meyer</p>
          </div>
        </div>
      </div>
    </div>
  );
};

const SermonLiist = () => {
  const { register } = useForm();

  return (
   <>
   <Navbar />
    <Hero>
      

      <div className="border-b-2 flex justify-between text-txtColor mt-40 p-3">
        <p className="font-bold ">Jan 24, 2020.</p>

        <p className="font-bold ">Johnson Meyer</p>
      </div>
      <h1 className="font-black text-2xl text-txtColor mt-4">
        Gather: Build Authentic Relationships with God
      </h1>

      <div className="main_sermon_frame w-8p border mx-auto my-20">
        <img src={main} alt="" className="" />
      </div>
      </Hero>

      <Sermons>
        <h1 className="title  font-bold p-2 mr-auto relative text-center w-52    text-2xl text-txtColor capitalize">
          Latest Sermons
        </h1>

        <div className="gallery grid-cols-1 grid md:grid-cols-3 w-full border gap-8 mt-16">
          {[1, 1, 1, 1, 1, 1, 1, 1, 1].map((item, index) => (
            <SermonCard id={index} />
          ))}

          
        </div>
        <div className="w-full flex justify-center mt-14"> 
          <Button text="Next" width="130px" height="48px"/>
          </div>

      </Sermons>

      <Footer />
    
   </>
  );
};

export const Hero = styled.div`
  
  padding: 1rem 5rem;
  .hero {
    background-image: url(${hero});
    background-repeat: no-repeat;
    height: 90vh;
  }
`;

export const Sermons = styled.section`
  width: 100%;
  /* height: 100vh; */
  background: #f9f9fa;
  padding: 3rem 7rem;
  position: relative;
  /* border:1px solid red; */

  .image_cover {
    background: #3a363d;
    font-family: "Merriweather", serif;
    .date {
      color: #c883f2;
    }
  }

  .sermon-cards {
    border-color: #880ed4;
  }

  .list_item {
    border-bottom: 2px solid #8d538d;
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;
  }
`;

export default SermonLiist;
