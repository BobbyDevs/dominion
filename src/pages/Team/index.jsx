import React from "react";
import { Hero, AboutUs, TeamCard} from "./styles";

import pastor1 from "../../assets/pastor1.png";
import { FaFacebookF } from "react-icons/fa";
import { GrMail } from "react-icons/gr";

function toggleLayer(id) {
  const overlayCard = document.getElementById(id);
  overlayCard.classList.toggle("active");
}

const ProfileCard = ({ id }) => {
  return (
    <div className="card-one  relative">
      <img src={pastor1} alt="" className="cursor-pointer w-full h-full" onClick={() => toggleLayer(id)} />
      <div
        id={id}
        onClick={() => toggleLayer(id)}
        className="overlayCard  flex justify-center items-center absolute  w-full  bottom-0"
      >
        <div className="icon_c flex">
          <div className="messages flex cursor-pointer justify-center items-center rounded-full bg-white w-11 h-11">
            <FaFacebookF className="text-txtColor" />
          </div>

          <div className="messages ml-3 flex cursor-pointer justify-center items-center rounded-full bg-white w-11 h-11">
            <GrMail className="text-txtColor" />
          </div>
        </div>
      </div>
    </div>
  );
};

const Team = () => {
  return (
    <Hero>
      <div className="hero w-full h-screen relative">
     
      <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-4xl md:text-6xl about_us">Our Team </h1>
        </div>
      </div>

      {/*  */}
      <AboutUs className="about_us_text border">
        <h1 className="title  font-bold p-2 md:mx-auto relative text-left md:text-center w-48 text-3xl text-txtColor capitalize">
          the team
        </h1>

        <p className="one text-txtColor md:mx-auto mt-6 font-normal md:w-8p text-left md:text-center">
          We believe God’s plan is that each local church be led by a team of
          qualified, spiritually mature men and women whose proven leadership in
          their home and community demonstrates their ability to lead God’s
          church..
        </p>
      </AboutUs>
      {/*  */}
      <TeamCard>
        <div className="grid grid-cols-2 md:grid-cols-3 gap-11">
          {[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map((item, index) => (
            <ProfileCard id={index} />
          ))}
        </div>
      </TeamCard>

     
    </Hero>
  );
};

export default Team;
