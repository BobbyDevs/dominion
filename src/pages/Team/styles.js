import styled from "styled-components";
import hero from "../../assets/hero_1.webp";


export const Hero = styled.div`
  .hero {
    background-image: url(${hero});
    height:90vh;


    .hero_content {
      h1 {
        font-family: "Merriweather", serif;
      }
    }

    .subtext {
      color: #f5f6f8;
    }

    .join_us_container {
      margin-left: 11rem;
      width: 990px;
    }

    .join_us {
      width: 900px;

      .card {
      }
    }
  }
`;


export const AboutUs = styled.section`
  width: 100%;
  height: 40vh;
  background: #fff;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;
  @media screen and (max-width: 600px) {
  padding: 5rem 1.5rem;
    }


  .content {
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;

    @media screen and (max-width: 600px) {
      left: 40px;
    }
  }
`;



export const TeamCard = styled.section`
  width: 100%;
  height: 100%;
  background: #F9F9FA;
  /* background: #f9f6fb; */
  padding: 5rem;
  position: relative;

  @media screen and (max-width: 600px) {
  padding: 5rem 1.5rem;
    }

  .content {
  }

  .title:before {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 50%;

    transform: translateX(-50%);
    width: 40%;
    height: 3px;
    background: #991ee4;
  }


  .card-one{
    box-shadow:15px 18px 0 0 #D7A1F9;


    .overlayCard{
      transition: All 0.4s linear;
      height:0;
      background:#22053462;

      .icon_c{
        opacity:0;
      transition: All 0.2s linear;

        
      }
      

      &.active{
        height:100%;
      transition: All 0.2s linear;
      .icon_c{
        opacity:1;

      }

      }
    }
  }
`;

