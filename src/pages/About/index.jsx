import React from "react";
import { Hero, AboutUs, Mandate, Values } from "./style";
// import Navbar from "../../components/Navbar";
import cross from "../../assets/cross.png";
import cross2 from "../../assets/cross2.png";
import cross3 from "../../assets/cross3.png";
// import Footer from "../../components/common/footer";
import scrollToTop from "../../components/scrollToTop";

const About = () => {
  return (
    <>
      <scrollToTop/>
    <Hero>
      <div className="hero w-full h-screen relative">
      
        <div className="hero_content absolute top-1/2 left-4 md:left-40">
          <h1 className="capitalize font-medium text-4xl md:text-6xl about_us">about Us</h1>
        </div>
      </div>

      {/*  */}
      <AboutUs className="about_us_text">
        <h1 className="font-bold p-2 md:mx-auto relative text-center w-40 text-3xl text-txtColor capitalize">
          About us
        </h1>

        <p className="one text-txtColor md:ml-40 mt-6 font-normal md:w-8p text-left md:text-center">
          Dominion Christian Centre is a growing charismatic church based in the
          Docklands area of London, England. At DCC, we believe that God has
          ordained a future for everyone which they have to possess. At DCC, we
          preach Jesus Christ and lives of many are transformed.
        </p>
      </AboutUs>
      {/*  */}

      <Mandate>
        <div className="row flex flex-col md:flex-row justify-between w-full md:h-4p mt-12 font-normal">
          <div className="image_container rounded-lg md:h-4p md:w-6p relative">
            <img src={cross} alt="" className="md:w-5p" />
          </div>
          <div className="sermon_list  h-full md:w-4p text-txtColor text-sm">
            <div className="list-item-conainer  mt-4">
              <h1 className="capitalize font-semibold text-3xl title">our mandate</h1>
              <p className="text mt-8 leading-6">
                DCC started in March 1999. God called Pastor Yinka Popoola with
                3 basic mandates which formed the foundation of our Vision and
                Mission as a Church.
              </p>
              <p className="list-item-conainer  mt-4">
                The three mandates are:{" "}
              </p>
              <ol className="more_info  ml-3 leading-6">
                <li className="lists">
                  Go and set the captives free. (Matt.16:19; Acts 10:38;
                  Luk.4:18)
                </li>
                <li className="lists">
                  He is sending me out as a reaper on the field of life in
                  Britain. (John 4:34-38)
                </li>
                <li className="lists">
                  Revive the people of God by teaching them to pray
                  prophetically using God’s word and subsequently revive the
                  land by the power of the Holy Ghost. 1Sam.3:11; Isa.41:18-19.
                </li>
              </ol>
            </div>
          </div>
        </div>
      </Mandate>

      <Mandate>
        <div className="row flex flex-col-reverse md:flex-row-reverse justify-between w-full md:h-4p md:mt-12 ">
          <div className="image_container rounded-lg md:h-4p md:w-5p relative mt-8 md:mt-0">
            <img src={cross2} alt="" className=" w-5p" />
          </div>
          <div className="sermon_list  h-full md:w-4p5 text-txtColor text-sm">
            <div className="list-item-conainer mt-4">
              <h1 className=" capitalize font-bold text-3xl">Our Commitments</h1>
              <p className="text leading-7">
                1. Winning the lost and attracting backsliders back to God
                <br />
                2. Equipping the saints to excel and do the work of the ministry
                <br />
                3. Mentoring and releasing competent leaders into the body of
                Christ
              </p>
            </div>
          </div>
        </div>
      </Mandate>

      {/*  */}

      <Values>
        <div className="row flex-col md:flex-row flex justify-between w-full md:h-4p mt-8 md:mt-12 ">
          <div className="image_container hidden md:block rounded-lg md:h-4p md:w-6p relative">
            <img src={cross3} alt="" className="w-5p" />
          </div>

          <div className="sermon_list  h-full md:w-5p text-txtColor text-sm">
            <div className="list-item-conainer  mt-4">
              <h1 className="capitalize font-bold text-3xl">our core values</h1>
              <p className="text mt-4">
                Authentic members of Dominion Christian Centre commit to:
              </p>

              <p className="text mt-4 leading-6">
                <b>P</b>. = Live a Purpose-driven life
                <br />
                <b>R</b>. = Have Respect for people
                <br />
                <b>E</b>. = Excellence honours God and inspires greatness
                <br />
                <b>A</b>. = Adoration Leads To Restoration
                <br />
                <b>C</b>. = Love means sacrifice and Commitment
                <br />
                <b>H</b>. = Humility brings promotion
                <br />
                <b>C</b>. = Have Compassion for the lost
                <br />
                <b>H</b>. = Have love for Humanity
                <br />
                <b>R</b>. = live a Righteous life
                <br />
                <b>I</b>. = be an Inspiration to others
                <br />
                <b>S</b> = Strengthen the weak T = enforce Total victory over
                sin
              </p>
            </div>
          </div>
        </div>
      </Values>
      {/*  */}

     
    </Hero>
    </>
  );
};

export default About;
