import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import InputField from "../../components/common/InputField";
import Logo from "../../assets/logo.png";
import Button from "../../components/common/Button";
import { AuthPage } from "./style";
import { userContext } from "../../context/userContext";

// _______________________________________________________________________
const Auth = ({ toggleUser }) => {
  const { dispatch } = useContext(userContext);

  const [forgotPassWord, setFpassword] = useState(false);
  const { register, handleSubmit } = useForm();

  function toggleFpassword() {
    setFpassword((prev) => !prev);
  }


  return (
    <AuthPage>
      <div className="auth_content">
        <div className="logo_container border">
          <img src={Logo} alt="ajoo logo" />
        </div>

        <form
       
          onSubmit={handleSubmit((data) =>
            dispatch({ type: "LOG_IN", payload: data })
          )}
        >
          {forgotPassWord && (
            <p>
              Please enter your email address below and a password reset link
              will be sent to you
            </p>
          )}

          <InputField
            type="email"
            register={register}
            name="email"
            icon="user"
            req
            placeholder="example@gmail.com"
          />

          {!forgotPassWord && (
            <>
              <InputField
                type="password"
                register={register}
                name="password"
                placeholder="password"
                req
              />

              <span onClick={toggleFpassword}>Forgot password?</span>
            </>
          )}

          <Button
            active
            height="42px"
            mT="0.8rem"
            type="submit"
            rad="5px"
            text={!forgotPassWord ? "Log in" : "reset password"}
            />



        </form>
{
  forgotPassWord && (
    
    <h1 className='font-medium mt-7 text-txtColor'>
    Remember your Password? <span onClick={toggleFpassword} className="text-primary font-bold cursor-pointer">Log in</span>
</h1>
  )
}
      </div>
    </AuthPage>
  );
};

export default Auth;
