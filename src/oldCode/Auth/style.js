import styled from "styled-components";

export const AuthPage = styled.div`
display: flex;
justify-content: center;
align-items: center;
width: 100%;
height: 100vh;
background-color: #fff;

.auth_content {
    width: 360px;
    height:310px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;

    img{
        width:80px;
        height:80px;
    }

    form {
        margin-top: 1.5rem;
        width: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;

        p {
            color: #370352;
            font-weight: normal;
            font-size: 14px;
            line-height:16.4px;
            margin:0.8rem 0;
            margin-bottom: 20px;
        }

        span {
            align-self: flex-end;
            text-decoration: none;
            font-weight: 600;
            color: #52057b;
            cursor: pointer;
        }
    }
}
`;
