import React from 'react'
import styled from "styled-components";


const ModalFormInput = ({ read,value, label, type, name, register, icon, req, placeholder }) => {
    return (
        <Container>
        <label htmlFor={name}>{label}</label>
        {
          value ? <input className="b-l" readOnly value={value} type="text" />:(
            <input 
            id={name}
            type={type}
           
            {...register(name, { required: req ? true : false })}
            placeholder={placeholder}
            readOnly={read}
          />
          )
        }
      </Container>
    )
}

const Container = styled.div`
  width:250px;
  display: flex;
  flex-direction: column;
  margin:0rem 15px;
  label {
    color: #687C97;
    font-size: 12px;
  }

  input {

    border: 1px solid #687C97;
    padding: 1rem;
    font-size: 14px;
    width: 100%;
    border-radius: 10px;
    margin-top: 10px;
    height:40px;
    color: #29023D;
    &:focus {
      outline: none;
    }
    
    &::placeholder{
      color:#B9C2C9;
    }
  }

  .b-l{
    border:none;
    font-size:16px;
    color:#1B0229;
    font-weight: 500;
    line-height:12px;
    height:30px;
    margin-bottom:1.4rem;
  }
`;

export default ModalFormInput
