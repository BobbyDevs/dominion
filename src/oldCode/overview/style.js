import styled from "styled-components";

export const Container = styled.div`
  padding: 4rem;
  background: #fbf9fc;
  width: 100%;
  margin-top:-30px;

  .card_container {
    width: 100%;
    height: 120px;
    display: flex;
    justify-content: space-between;

    .card {
      width: 235px;
      border: 1px solid #dccde5;
      border-radius: 10px;
      background-color: #fff;
      display: flex;
      flex-direction: column;
      align-items: center;
      padding:5px;
      // box-shadow: #dccde5 0px 0px 5px 0px, #dccde5 0px 0px 1px 0px;

      h1 {
        font-size: 26px;
        margin-top: 0rem;
        color: #100119;
        text-align:left;
        width:100%;
        font-weight:600;
        margin-left:8.5rem;
      }

      .card_title {
        width: 90%;
        margin: 0 auto;
        display: flex;
        align-items: center;
        margin-top:1rem;

        .icon_cont {
          border: 1px solid #f6f8fa;
          border-radius: 50%;

          width: 40px;
          height: 40px;
          box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 50px;
          .iconify {
            font-size: 24px;
            color:#555555;
          }
        }

        p {
          text-transform: uppercase;
          font-size: 13px;
          color: #555555;
          margin-left:1rem;
        }
      }
    }
  }

  .section {
    display: flex;
    /* justify-content: space-between; */
    margin: 1rem 0;
    height: 450px;
    min-width:80%;

    .b{
      border:1px solid red;

    }

    .chart_box {
      /* width: 640px; */
      background: #fff;
      height: 480px;
      border-radius: 10px;
      border: 1px solid #dccde5;

      .header{
        
        
        height:70px;
        h2{
          color:#222B45;
          font-size:16px;
        }

        .total{
          h1{
            font-size: 19px;
            font-weight:600;
          }
          p{
            color:#687C97;
            font-size:15px;
          }
        }

        .tabs{
          a{
             border:1px solid transparent;
             width:100%;
             height:100%;
             color:#687C97;
             font-weight: 500;
             

             &:hover, &.active {
               color:#52057B;
               border-bottom-color:#52057B;
             }
          }
        }
      }
    }


  }
`;
