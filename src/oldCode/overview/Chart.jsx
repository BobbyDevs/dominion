import React from "react";
import { Bar } from "react-chartjs-2";
import styled from "styled-components";

let chartData = {
  labels: ["Mon", "Tues", "Wed", "Thus", "Fri", "Sat", "Sun"],
  datasets: [
    {
      label: "Numbers of Contribution",
      data: [1200, 1100, 1300, 500, 900, 1500, 2000],
      backgroundColor: "#52057B",
      barThickness: 10,
      borderRadius: 10,
    },
  ],
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  },
};

const Chart = () => {
  return (
    <Wrapper>
      <Bar data={chartData} height={100}/>
    </Wrapper>
  );
};

const Wrapper = styled.div`
      padding:1.5rem;
      padding-top:0;

`;
export default Chart;
