import React from "react";
// import Add from "../../assets/add.png";
import { NavLink, Switch, Route } from "react-router-dom";
import { Container } from "./style";
import Chart from "./Chart";

const Overview = () => {
  return (
    <Container className="border">
      <div className="card_container">
        <div className="card">
          <div className="card_title">
            <span className="centralize icon_cont">
              <span
                class="iconify"
                data-icon="mdi:cart-outline"
                data-inline="false"
              ></span>
            </span>
            <p className="text">total user</p>
          </div>
          <h1 className="price">400</h1>
        </div>

        <div className="card">
          <div className="card_title">
            <div className="centralize icon_cont">
			<span class="iconify" data-icon="bx:bx-money" data-inline="false"></span>
            </div>
            <p className="text">total contributions</p>
          </div>
          <h1 className="price">N4,000.28</h1>
        </div>

        <div className="card">
          <span className="card_title">
            <div className="centralize icon_cont">
			<span class="iconify" data-icon="mdi:account-group" data-inline="false"></span>
            </div>
            <p className="text">total groups</p>
          </span>
          <h1 className="price">24</h1>
        </div>

        <div className="card">
          <span className="card_title">
            <div className="centralize icon_cont">
              <span
                class="iconify"
                data-icon="ant-design:sync-outlined"
                data-inline="false"
              ></span>
            </div>
            <p className="text">ACTIVE CONTRIBUTION</p>
          </span>
          <h1 className="price">24</h1>
        </div>
      </div>

      <div className="section">
        {/* chart section begins */}
        <div className="chart_box w-full">
          <di v className="header border-b flex justify-between items-center ">
            <h2 className="ml-5 font-semibold p-2">Revenue</h2>
            <nav className="tabs mr-5 w-48 h-full flex">
              <NavLink className="items-center justify-center flex" to="/kk">
                Day
              </NavLink>
              <NavLink
                className="items-center justify-center flex"
                to="/overview"
              >
                Week
              </NavLink>
              <NavLink className="items-center justify-center flex" to="/uu">
                Month
              </NavLink>
            </nav>
          </di>

          <div className="header mt-3  flex justify-between items-center">
            <div className="total ml-7">
              <h1 className="balance mb-2  text-gray-900">$12,000</h1>
              <p className="cash">Earning over time</p>
            </div>

            <div className="total mr-7">
              <h1 className="balance  text-right mb-2 text-gray-900">2265</h1>
              <p className="cash">Total Contributions</p>
            </div>
          </div>
          <Switch>
            <Route path="/overview">
              <Chart />
            </Route>
          </Switch>
        </div>
        {/* chart section ends */}

        {/* <div className='groups'>
					<div className='group_header'>
						<h2>Groups</h2>
						<img src={Add} alt='hello world' />
					</div>

					<div className='group_table'>
						<div className='group_table-header'>
							<div className="th_cell contribution">Name</div>
							<div className="th_cell contribution">Total contribution</div>
							<div className="th_cell centralize info"><span>Update info</span></div>
						</div>
						{Arr.map((item) => (
							<div className='group_table-body'>
								<div className="td_cell ">{item.name}</div>
								<div className='td_cell price'>{item.price}</div>
								<div className='td_cell centralize info'>
									<span
										class='iconify'
										data-icon='mdi:square-edit-outline'
										data-inline='false'></span>
								</div>
							</div>
						))}
					</div>
				</div> */}
      </div>
    </Container>
  );
};

export default Overview;
