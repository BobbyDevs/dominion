import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  padding: 2rem;
  background: #fbf9fc;
  margin-bottom: 20px;

  div.title_bar {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: -1rem;

    h1 {
      color: #222b45;
    }
  }

  div.user_list {
    margin-top: 1rem;
    width: 100%;
    background: #fff;
    position: rlative;

    .header-bar {
      height: 55px;
      margin-bottom: 0px;

      background-color: #fbf9fc;
      min-width: calc(100% - 240px);

      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid #edf2f6;

      .search_container {
        position: relative;
        width: 300px;
        height: 36px;
        background: #fff;
        margin-left: 30px;
        border-radius: 8px;
        border: 1px solid #f4f7f9;

        .icon {
          position: absolute;
          left: 0;
          width: 40px;
          height: 100%;
          .iconify {
          }
        }

        input {
          position: absolute;

          width: calc(100% - 40px);
          padding-left: 5px;
          position: absolute;
          left: 40px;
          height: 100%;
          background: transparent;
          border: none;

          &:focus {
            outline: none;
          }
        }
      }

      .sort_container {
        margin-right: 30px;
        /* border:1px solid red; */
        position: relative;
        width: 300px;
        height: 36px;
        background: #fff;
        border-radius: 8px;
        border: 1px solid #f4f7f9;

        select {
          padding-left: 1rem;
          border: none;

          &:focus {
            outline: none;
          }
        }
      }
    }

    .tables {
      overflow-y: scroll;
      height: 80vh;
      position: relative;

      &::-webkit-scrollbar {
        width: 2px; /* width of the entire scrollbar */
      }
      /* Track */
      &::-webkit-scrollbar-track {
        background: #fff;
        /* border: 0.33px solid rgba(255, 255, 255, 0.1); */
      }
      /* Track */

      /* Handle */
      &::-webkit-scrollbar-thumb {
        background: #dccde5;
      }

      /* Handle on hover */
      &::-webkit-scrollbar-thumb:hover {
        background: #555;
      }
    }

    .user_list {
      height: 100%;
      width: 100%;
      position: absolute;
      top: 0;
      margin-top: -5px;

      &-header {
        position: sticky;
        top: 0;
        bottom: 0;
        height: 50px;
        display: flex;
        width: 100%;
        /* border:1px solid green; */

        .th_cell {
          width: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          color: #1b0229;
          font-family: "Roboto", sans-serif;
          font-weight: 600;
          font-size: 14.9px;
          background: #dccde5;
          padding: 1.6rem;

          &.info {
            width: 100%;
          }
          &.s_n {
            width: 40%;
          }
        }
      }

      .user_table-body {
        height: 65px;
        display: flex;
        width: 100%;

        .td_cell {
          width: 100%;

          display: flex;
          align-items: center;
          justify-content: center;
          color: #222b45;
          border: 1px solid #edf2f6;
          font-family: "Roboto", sans-serif;
          font-weight: 400;
          font-size: 13.7px;
          background: #fff;
          padding: 2.0rem;
          position: relative;

          button {
      width: 135px;
      height: 38px;
      background: rgba(220, 205, 229, 0.4);
      color: #52057b;
      border: none;
      border-radius: 10px;
      font-size: 15px;
      font-weight: 500;
      cursor: pointer;
    }

          &.info {
            width: 100%;
            .iconify{
              margin-left:1rem;
              font-size:17px;
              color:#687C97;
            }

           

            .toothlip {
              display: none;
              top: 0;
              position: absolute;
              padding: 0.8rem;
              top: 47px;
             left:22px;
             width:200px;
              background: #fff;
              border: 1px solid #edf2f6;
              box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px,
                rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
              border-radius: 10px;
              z-index:3;

              .iconify{
                color:#29023D;
              }

              &.active{
                  display:block;
                }

              &:before {
                content: "";
                position: absolute;
                border-width: 10px;
                border-style: solid;
                border-color: transparent transparent #fff transparent;
                left: 50%;
                top: -20px;
                transform: translateX(-50%);
              }

              .title {
                border-bottom: 1px solid #eff4fa;
                h2 {
                  margin: 5px;
                  color: #222b45;
                  font-size:19px;
                  font-weight:600;

                }
              }
            }
          }
          &.s_n {
            width: 37%;
          }
          &.name{
            justify-content: flex-start;
          }
        }
      }
    }
  }
`;

export const ListContainer = styled.div`
background: #fbf9fc;

.container{


width:90%;
height:85vh;



.role_list{
  background: #fff;
  margin-bottom: 20px;

  .list {
      height: 100%;
      width: 100%;
      

      .search_container {
        position: relative;
        width: 250px;
        height: 36px;
        background: #fff;
        border-radius: 8px;
        border: 1px solid #f4f7f9;

        .icon {
          position: absolute;
          left: 0;
          width: 40px;
          height: 100%;
          .iconify {
          }
        }

        input {
          position: absolute;
          font-size:12px;
          width: calc(100% - 40px);
          padding-left: 5px;
          position: absolute;
          left: 40px;
          height: 100%;
          background: transparent;
          border: none;

          &:focus {
            outline: none;
          }
        }
      }


      &-header {
        position: sticky;
        top: 0;
        bottom: 0;
        height: 50px;
        display: flex;
        width: 100%;
        /* border:1px solid green; */

        .th_cell {
          width: 100%;
          display: flex;
          align-items: center;
          color: #1b0229;
          font-family: "Roboto", sans-serif;
          font-weight: 600;
          font-size: 14.9px;
          background: #dccde5;
          padding-left: 0.6rem;

          &.info {
            width: 100%;
          }
          &.s_n {
            width: 20%;
          }
        }
      }

      .user_table-body {
        height: 50px;
        display: flex;
        width: 100%;

        .td_cell {
          width: 100%;
          display: flex;
          align-items: center;
          color: #222b45;
          border: 1px solid #edf2f6;
          font-family: "Roboto", sans-serif;
          font-weight: 400;
          font-size: 13.7px;
          background: #fff;
          padding-left: 0.6rem;
          position: relative;

          &.info {
            width: 100%;
            .iconify{
              margin-left:1rem;
            }

            &:hover {
              .toothlip {
                display: block;
              }
            }

            .toothlip {
              display: none;
              top: 0;
              position: absolute;
              padding: 0.8rem;
              top: 47px;
             left:-60px;
             width:180px;
              background: #fff;
              border: 1px solid #edf2f6;
              box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px,
                rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
              border-radius: 10px;
              z-index:3;

              &:before {
                content: "";
                position: absolute;
                border-width: 10px;
                border-style: solid;
                border-color: transparent transparent #fff transparent;
                left: 50%;
                top: -20px;
                transform: translateX(-50%);
              }

              .title {
                border-bottom: 1px solid #eff4fa;
                h2 {
                  margin: 5px;
                  color: #222b45;
                  font-size:19px;
                  font-weight:600;

                }
              }
            }
          }
          &.s_n {
            width: 20%;
          }
        }
      }
    }
  }
}
}


`
