

import React from "react";
import styled from "styled-components";
import { IoMdClose } from "react-icons/io";
import { useForm } from "react-hook-form";
import ModalFormInput from "../../components/common/ModalFormInput";
import Button from "../../components/common/Button";

const RoleModal = ({ showModal, content, isUpdating, modifying }) => {
  const { register } = useForm();


  return (
    <ModalLayer className="centralize">
      <div className="card">
        <div className="card_title ">
        {
          modifying 
          ? <h1 className="font-bold text-txtColor text-xl p-3">Modify Permissions</h1>
          : <h1 className="font-bold text-txtColor text-xl p-3">{!isUpdating ? "Create New Role" : "Update Role Title"}</h1>
        }
          
          <IoMdClose onClick={showModal} className="icon " />
        </div>
        <p className="my-6 mb-0">Please enter full information bellow</p>

        {/* card form */}
        <form className="form_card">



         {
             !isUpdating && (
               <>
                {
                 !modifying
                  ? (
                    <ModalFormInput
                label="Role Title"
                register={register}
                name="roleTitle"
                type="text"
                placeholder="e.g bobby crooz"
              />
                  ) : (
                    <ModalFormInput
              label="Role Title"
              register={register}
              name="formerRoleTile"
              type="text"
              read
              value="Administrators"
            />
                  )
                }


              <div className="permissions mt-4">
                  <p>Permissons</p>
                  <div className="input_container">
                      <input type="checkbox" name="editAccess" id="editAccess" />
                      <label htmlFor="editAccess">Edit Access</label>
                  </div>

                  <div className="input_container">
                      <input type="checkbox" name="editAccess" id="editAccess" />
                      <label htmlFor="editAccess">Eleifend.</label>
                  </div>

                  <div className="input_container">
                      <input type="checkbox" name="editAccess" id="editAccess" />
                      <label htmlFor="editAccess">Remove/Add Users</label>
                  </div>

                  <div className="input_container">
                      <input type="checkbox" name="editAccess" id="editAccess" />
                      <label htmlFor="editAccess">Congue tincidunt.</label>
                  </div>
              </div>
               </>
             )
         }

          {isUpdating && (
         <>
            <ModalFormInput
              label="Former Role Title"
              register={register}
              name="formerRoleTile"
              type="text"
              read
              value="Administrators"
            />


            <ModalFormInput
            label="New Role Title"
            register={register}
            name="newRoleTitle"
            type="text"
            placeholder="example@gmail.com"
          />

         </>
       
          )}

          <div className="button_cont centralize">
            <Button height="40px" width="139px" text={ !isUpdating && !modifying ? "Create Role" : "Save Changes"} active />
            <h3 className="centralize" onClick={showModal}>Cancel</h3>
          </div>
        </form>
      </div>
    </ModalLayer>
  );
};

const ModalLayer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 4;
  width: 100%;
  height: 100%;
  background: #031021a9;
  color:#687C97;

  .card {
    width: 400px;
    /* height: 553px; */
    background: #fff;
    border-radius: 15px;
    /* z-index: 5; */

    p {
      text-align: center;
      font-size: 15px;

    }

    &_title {
      width: 100%;
      height: 60px;
      border-bottom: 1px solid #EFF4FA;
      display: flex;
      justify-content: space-between;
      align-items: flex-end;
      margin-top: 1.5rem;
    }

    h1 {
      /* margin: 5px; */
      margin-left: 20px;
      margin-bottom: 5px;
    }

    .icon {
      /* border: 1px solid red; */
      margin-right: 25px;
      margin-bottom: 20px;
      font-size:24px;
    }
  }
  /* form card */
  .form_card {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin:2rem 0;

    .permissions{
        width:250px;

        p{
            text-align: left;
            color: #687C97;
        }

        .input_container{
        margin:5px;

        label{
            color:#29023D;
            font-size:13;
            line-height: 20px;
            letter-spacing: -0.09px;
            font-weight:400;
            margin-left:10px;
        }

        input[type="checkbox"]:checked{
            background-color: red;
            border:1px solid red;
        }

        }
    }

    .button_cont {
      width: 100%;
      display: flex;
      justify-content: center;
      margin-top: 20px;


      h3{
          width:129px;
          height:40px;
          cursor: pointer;
      }
    }
  }

  .button_cont {
    width: 100%;
    display: flex;
    justify-content: center;
    margin-top: 9px;
  }
`;

// const Card = styled.div``;

export default RoleModal;
