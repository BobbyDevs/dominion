import React from "react";
import styled from "styled-components";
import { IoMdClose } from "react-icons/io";

const ActionModal = ({ toggle, item }) => {
  return (
    <ModalLayer className="centralize">
      <div className="card">
        <div className="card_title border-b h-24 p-12 pb-3  font-black  text-txtColor flex justify-between items-center">
          <h1>Delete Role</h1>
          <div className="icon cursor-pointer " onClick={toggle}>
            <IoMdClose className="w-9 h-5 " />
          </div>
        </div>

        <div className="content w-full mt-3 centralize">
          <p className="text-txtColor mt-4 text-center">
            Are you sure you want to Delete
            <br />
            <span>Bobby Crooz?</span>
          </p>
        </div>
        <div className="button_container flex justify-center items-center mt-8">
          <button className="deactivate action p-4 rounded-2xl h-12 centralize w-40">
            Delete Role
          </button>

          <h2
            onClick={toggle}
            className="cancel text-primary cursor-pointer p-4"
          >
            Cancel
          </h2>
        </div>
      </div>
    </ModalLayer>
  );
};

const ModalLayer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 4;
  width: 100%;
  height: 100%;
  background: #031021a9;

  .card {
    width: 450px;
    height: 320px;
    background: #fff;
    border-radius: 15px;
    h1 {
      font-size: 20px;
    }

    .deactivate {
      background-color: rgba(204, 0, 0, 0.24);
      font-weight: 500;
      color: red;
      border-radius:10px;
    }

  
    .content {
      p {
        width: 280px;
        height: 40;
        color: #39154b;
        font-size: 17px;
        span {
          font-weight: 600;
        }
      }
    }
  }
`;

export default ActionModal;
