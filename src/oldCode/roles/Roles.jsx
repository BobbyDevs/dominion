import React, { useState } from "react";
import Button from "../../components/common/Button";
import { Container, ListContainer } from "./style";
// import RoleCard from "./RoleCard";
import RoleModal from "./RoleModal";
import Data from "./RolesData";
import { BsArrowLeft } from "react-icons/bs";
import { BiChevronRight } from "react-icons/bi";
import ActionModal from "./ActionModal";

// onClick={()=> handleAction(item) }
const RoleList = ({ RoleItem, viewList }) => {
  return (
    <React.Fragment>
      <ListContainer>
        <div className="container ml-10">
          <div
            onClick={viewList}
            className="bac_btn flex items-center text-txtColor font-normal text-sm"
          >
            <BsArrowLeft />
            <span className="ml-2 cursor-pointer">Back</span>
          </div>
          <div className="mt-4 flex items-center head text-txtColor2 font-normal text-3xl">
            Roles
            <BiChevronRight />
            <span className="font-meddium text-sm">{RoleItem.name}</span>
          </div>

          <div className="list min-w-screen mt-4">
            <main className="role_list">
              <div className="list">
                <div className="header-bar flex items-center justify-between py-1">
                  <h2 className="text-txtColor text-md font-semibold ml-4">
                    {RoleItem.name}
                  </h2>

                  <div className="search_container mr-4">
                    <div className="icon centralize">
                      <span
                        class="iconify"
                        data-icon="akar-icons:search"
                        data-inline="false"
                      ></span>
                    </div>
                    <input type="search" placeholder="Search User.." />
                  </div>
                </div>

                <div className="tables">
                  <div className="list">
                    <div className="list-header">
                      <div className="th_cell s_n">S.No</div>
                      <div className="th_cell border ">Name</div>
                      <div className="th_cell">Email address</div>
                      <div className="th_cell border ">Permissions</div>

                      <div className="th_cell info">Actions</div>
                    </div>

                    {[1, 2, 1, 2, 1, 2].map((item, index) => (
                      <div className="user_table-body">
                        <div className="td_cell s_n">{index + 1}</div>
                        <div className="td_cell name">
                          <div className="flex items-center ">
                            <img src={Oval} alt="" />
                            <p className="ml-4">bobby</p>
                          </div>
                        </div>
                        <div className="td_cell phone">random content</div>
                        <div className="td_cell email">random content</div>

                        <div
                          onClick=""
                          className="td_cell cursor-pointer info"
                          key={`user-${index}`}
                        >
                          {/* tootip */}
                          {/* <div className="toothlip absolute">
                            <div className="title">
                              <h2>Action</h2>
                            </div>

                            <div className="action_cont p-1">
                              <div className="action_item flex items-center">
                                <span
                                  class="iconify"
                                  data-icon="ant-design:edit-filled"
                                  data-inline="false"
                                ></span>
                                <p className="ml-4 text-txtColor font-meddium">
                                  {item === 1
                                    ? "Deactivate User"
                                    : "Activate User"}
                                </p>
                              </div>
                            </div>
                          </div> */}
                          {/* tootip */}

                          <span
                            class="iconify"
                            data-icon="mdi:square-edit-outline"
                            data-inline="false"
                          ></span>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>
      </ListContainer>
    </React.Fragment>
  );
};

// ---------------------------------------------

const RoleItems = ({
  viewList,
  showModal,
  handleIsUpdating,
  handleModifying,
  handleDelete,
}) => {
  return (
    <React.Fragment>
      <Container>
        <div className="title_bar">
          <h1 className="font-medium ml-4 text-4xl">Roles</h1>
          <Button
            active
            mR="2rem"
            text="create New Role"
            click={showModal}
            width="auto"
            height="41px"
          />
        </div>

        <div className="user_list">
          <div className="header-bar">
            <div className="search_container">
              <div className="icon centralize">
                <span
                  class="iconify"
                  data-icon="akar-icons:search"
                  data-inline="false"
                ></span>
              </div>
              <input type="search" placeholder="Search User" />
            </div>
          </div>

          <div className="tables">
            <div className="user_list">
              <div className="user_list-header">
                <div className="th_cell s_n">S.No</div>
                <div className="th_cell border">Role Title</div>
                <div className="th_cell ">Number of Users</div>
                <div className="th_cell border">View</div>
                <div className="th_cell">Action</div>
              </div>

              {Data.map((item, index) => (
                <div className="user_table-body">
                  <div className="td_cell s_n">{index + 1}</div>
                  <div className="td_cell name text-txtColor font-medium">
                    {item.name}
                  </div>
                  <div className="td_cell phone">{item.admins}</div>
                  <div className="td_cell email">
                    <button onClick={() => viewList(item)}>View List</button>
                  </div>

                  <div
                  onClick={() => document.getElementById(`too-${index}`).classList.toggle('active')}
                    className="td_cell cursor-pointer info"
                    key={`user-${index}`}
                  >
                    {/* tootip */}
                    <div id={`too-${index}`} className="toothlip absolute">
                      <div className="title">
                        <h2>Action</h2>
                      </div>

                      <div className="action_cont">
                        <div className="action_item flex my-1 items-center">
                          <span
                            class="iconify"
                            data-icon="ant-design:edit-filled"
                            data-inline="false"
                          ></span>
                          <p
                            onClick={handleIsUpdating}
                            className="ml-3 text-txtColor font-meddium"
                          >
                            Update Role Title
                          </p>
                        </div>
                      </div>

                      {/*  */}

                      <div className="action_item my-2 flex items-center">
                        <span
                          class="iconify"
                          data-icon="ant-design:edit-filled"
                          data-inline="false"
                        ></span>
                        <p
                          onClick={handleModifying}
                          className="ml-4 text-txtColor font-meddium"
                        >
                          Modify Permission
                        </p>
                      </div>
                      {/*  */}
                      <div className="action_item my-2 flex items-center">
                        <span
                          class="iconify"
                          data-icon="ant-design:delete-filled"
                          data-inline="false"
                        ></span>
                        <p
                          onClick={handleDelete}
                          className="ml-4 text-txtColor font-meddium"
                        >
                          Delete Role
                        </p>
                      </div>
                      {/*  */}
                    </div>
                    {/* tootip */}

                    <span
                      class="iconify"
                      data-icon="mdi:square-edit-outline"
                      data-inline="false"
                    ></span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Container>
    </React.Fragment>
  );
};

// role default
const Roles = () => {
  const [states, setStates] = useState({
    createRole: false,
    isUpdating: false,
    viewList: false,
    modifying: false,
    isDeleting: false,
    list: null,
  });

  function handleCreateRole() {
    setStates({
      ...states,
      createRole: !states.createRole,
      isUpdating: false,
    });
  }

  function handleModifying() {
    setStates({
      ...states,
      createRole: !states.createRole,
      isUpdating: false,
      modifying: !states.modifying,
    });
  }

  function handleIsUpdating() {
    setStates({
      ...states,
      createRole: !states.createRole,
      modifying: false,
      isUpdating: true,
    });
  }

  function handleDelete() {
    setStates({
      ...states,
      createRole: false,
      modifying: false,
      isUpdating: false,
      isDeleting: !states.isDeleting,
    });
  }

  function handleViewList(list) {
    setStates({
      ...states,
      viewList: !states.viewList,
      list: list,
    });
  }

  return (
    <React.Fragment>
      {states.isDeleting && <ActionModal toggle={handleDelete} item={states.list} />}

      {states.createRole && (
        <RoleModal
          modifying={states.modifying}
          isUpdating={states.isUpdating}
          showModal={handleCreateRole}
        />
      )}

      {states.viewList ? (
        <RoleList RoleItem={states.list} viewList={handleViewList} />
      ) : (
        <RoleItems
          handleDelete={handleDelete}
          handleModifying={handleModifying}
          handleIsUpdating={handleIsUpdating}
          showModal={handleCreateRole}
          viewList={handleViewList}
        />
      )}
    </React.Fragment>
  );
};

export default Roles;
