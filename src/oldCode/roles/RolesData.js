const UserRoles = [
  {
    name: "Super Administrator",
    icon: (
      <span
        class="iconify"
        data-icon="ic:baseline-admin-panel-settings"
        data-inline="false"
      ></span>
    ),
    listLink: "",
    description:
      "Super Admin can edit groups, remove members, suspend members,apien risus nulla ante elit, nunc, feugiat platea. ante elit,nunc, feugiat platea.",
    admins: 234,
    list: [
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
    ],
  },

  {
    name: "Administrator",
    icon: (
      <span
        class="iconify"
        data-icon="ic:baseline-admin-panel-settings"
        data-inline="false"
      ></span>
    ),
    listLink: "",
    description:
      "Super Admin can edit groups, remove members, suspend members,apien risus nulla ante elit, nunc, feugiat platea. ante elit,nunc, feugiat platea.",
    admins: 234,
    list: [
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
    ],
  },

  {
    name: "User",
    icon: (
      <span class="iconify" data-icon="bx:bxs-user" data-inline="false"></span>
    ),
    listLink: "",
    description:
      "Super Admin can edit groups, remove members, suspend members,apien risus nulla ante elit, nunc, feugiat platea. ante elit,nunc, feugiat platea.",
    admins: 234,
    list: [
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
    ],
  },

  {
    name: "Support Staff",
    icon: (
      <span
        class="iconify"
        data-icon="bx:bx-support"
        data-inline="false"
      ></span>
    ),
    listLink: "",
    description:
      "Super Admin can edit groups, remove members, suspend members,apien risus nulla ante elit, nunc, feugiat platea. ante elit,nunc, feugiat platea.",
    admins: 234,
    list: [
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
      { user: "bobby", role: "Super Admin" },
    ],
  },
];

export default UserRoles;