import React, { useState } from "react";
import styled from "styled-components";

const RoleCard = ({ name, icon, listLink, description, admins }) => {
  const [showToothLip, setShowThowToothLip] = useState(false);

  function toggleToothlip() {
    setShowThowToothLip((prev) => !prev);
  }
  return (
    <Container>
      <div className="action">
        {showToothLip && (
          <div className="toothlip">
            <div className="title">
              <h2 onClick={toggleToothlip}>Action</h2>
            </div>

            <div className="action_cont">
              <div className="action_item">
                <span
                  class="iconify"
                  data-icon="fa-solid:file-upload"
                  data-inline="false"
                ></span>
                <p>update role title</p>
              </div>
              <div className="action_item">
                <span
                  class="iconify"
                  data-icon="ant-design:edit-filled"
                  data-inline="false"
                ></span>
                <p>modify prmission</p>
              </div>
              <div className="action_item">
                <span
                  class="iconify"
                  data-icon="ant-design:delete-filled"
                  data-inline="false"
                ></span>
                <p>delete role</p>
              </div>
            </div>
          </div>
        )}

        <div className="icon">{icon}</div>

        <div className="icon menu_icon" onClick={toggleToothlip}>
          <span
            class="iconify"
            data-icon="carbon:overflow-menu-horizontal"
            data-inline="false"
          ></span>
        </div>
      </div>

      <div className="text_box">
        <h1>{name}</h1>

        <p>{description}</p>
      </div>

      <div className="card_footer">
        <button>view list</button>
        <h3>{admins} Admins</h3>
      </div>
    </Container>
  );
};

const Container = styled.div`
  position: relative;
  width: 430px;
  height: 270px;
  padding: 1rem;
  background: #fff;
  border: 1px solid #edf2f6;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  &.left {
    margin-left: 6rem;
  }

  .action {
    display: flex;
    width: 100%;
    justify-content: space-between;

    .toothlip {
      position: absolute;
      padding: 1rem;
      top: 47px;
      width: 200px;
      right: -64px;
      background: #fff;
      border: 1px solid #edf2f6;
      box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px,
        rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
      border-radius: 10px;

      &:before {
        content: "";
        position: absolute;
        border-width: 10px;
        border-style: solid;
        border-color: transparent transparent #fff transparent;
        left: 50%;
        top: -20px;
        transform: translateX(-50%);
      }

      .title {
        border-bottom: 1px solid #eff4fa;
        h2 {
          margin: 8px;
          color: #222b45;
        }
      }

      .action_cont {
        margin-top: 5px;
        .action_item {
          display: flex;
          align-items: center;
          margin-left: 10px;
          margin: 4px 0;
          /* border:1px solid red; */

          .iconify {
            font-size: 17px;
            color: #222b45;
          }
          p {
            margin: 5px;
            color: #29023d;
            text-transform: capitalize;
            font-size: 14px;
          }
        }
      }
    }

    .iconify {
      font-size: 40px;
      color: #6f2f91;
      cursor: pointer;
    }
  }

  .text_box {
    padding: 0;

    h1 {
      margin: 0;
      color: #29023d;
      font-weight: 600;
    }

    p {
      margin: 0;
      margin-top: 10px;
      width: 90%;
      color: #979797;
    }
  }

  .card_footer {
    display: flex;
    justify-content: space-between;
    align-items: center;

    button {
      width: 135px;
      height: 38px;
      background: #dccde5;
      color: #52057b;
      border: none;
      border-radius: 10px;
      font-size: 18px;
      font-weight: 600;
      cursor: pointer;
    }

    h3 {
      color: #a98eb8;
    }
  }
`;

export default RoleCard;
