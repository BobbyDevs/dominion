import React, { useState } from "react";
import { Container } from "./style";
// import Button from "../../components/common/Button";

import Modal from "./Modal";
import ActionModal from "./ActionModal";
import Pagination from "../../components/Pagination";

const arrs = [
  1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1,
  2, 1, 2,
];

const Users = () => {
  const [createUser, setCreateUser] = useState(false);
  const [confirmAction, setConfirmAction] = useState(false);
  const [assigning, setAssigning] = useState(false);
  const [item, setItem] = useState(2);

  // pagianation constants
  const [currentPage, setcurrentPage] = useState(1);
  const [itemPerPage] = useState(8); //diplay 8 items per page

  const indexOfLastItem = currentPage * itemPerPage;
  const indexOfFirstItem = indexOfLastItem - itemPerPage;
  const currentItem = arrs?.slice(indexOfFirstItem, indexOfLastItem); //list of data to display per page(iteratable)

  function handleCreateUse() {
    setCreateUser((prev) => !prev);
  }

  function handleAction(item) {
    setAssigning(false);

    setItem(item);
    setConfirmAction((p) => !p);
  }

  function toggleSetAssigning(item) {
    setAssigning(true);
    setConfirmAction(true);
  }

  // set curent userList
  const paginate = (pageValue, id) => {
    setcurrentPage(pageValue);
    document.getElementById(id).classList.toggle("active");
  };
  return (
    <>
      {createUser && <Modal showModal={handleCreateUse} />}
      {confirmAction && (
        <ActionModal
          item={item}
          assigning={assigning}
          toggle={() => setConfirmAction((p) => !p)}
        />
      )}

      <Container>
        <div className="user_list container rounded-md">
          <div className="header-bar p-1">
            <div className="search_container">
              <div className="icon centralize">
                <span
                  class="iconify"
                  data-icon="akar-icons:search"
                  data-inline="false"
                ></span>
              </div>
              <input type="search" placeholder="Search User" />
            </div>
          </div>

          <div className="tables">
            <div className="user_list">
              <div className="user_list-header">
                <div className="th_cell border s_n">S.N</div>
                <div className="th_cell">Full name</div>
                <div className="th_cell border">Phone number</div>
                <div className="th_cell">Email address</div>
                <div className="th_cell border">User status</div>
                <div className="th_cell info">Update info</div>
              </div>

              {currentItem.map((item, index) => (
                <div className="user_table-body">
                  <div className="td_cell s_n">{index + 1}</div>
                  <div className="td_cell name">random content</div>
                  <div className="td_cell phone">random content</div>
                  <div className="td_cell email">random content</div>
                  <div className="td_cell status">
                    {item === 1 ? (
                      <>
                        <p className="ml-4 text-green-500"> Active</p>
                      </>
                    ) : (
                      <>
                        <p className="ml-4 text-red-400">deactivated</p>
                      </>
                    )}
                  </div>
                  <div
                    onClick={() =>
                      document
                        .getElementById(`too-${index}`)
                        .classList.toggle("active")
                    }
                    className="td_cell cursor-pointer info"
                    key={`user-${index}`}
                  >
                    {/* tootip */}
                    <div id={`too-${index}`} className="toothlip absolute">
                      <div className="title">
                        <h2>Action</h2>
                      </div>

                      <div className="action_cont p-1">
                        <div className="action_item flex items-center">
                          <span
                            class="iconify"
                            data-icon="ant-design:edit-filled"
                            data-inline="false"
                          ></span>
                          <p
                            className="ml-4 text-txtColor font-meddium"
                            onClick={() => handleAction(item)}
                          >
                            {item === 1 ? "Deactivate User" : "Activate User"}
                          </p>
                        </div>

                        <div className="action_item  mt-2 flex items-center ">
                          <span
                            class="iconify"
                            data-icon="gridicons:user"
                            data-inline="false"
                          ></span>
                          <p
                            className="ml-4 text-txtColor   font-meddium"
                            onClick={toggleSetAssigning}
                          >
                            Assign Role
                          </p>
                        </div>
                      </div>
                    </div>
                    {/* tootip */}

                    <span
                      class="iconify"
                      data-icon="mdi:square-edit-outline"
                      data-inline="false"
                    ></span>
                  </div>
                </div>
              ))}
            </div>
          </div>

          <Pagination
            items={arrs}
            itemPerPage={itemPerPage}
            paginate={paginate}
            currentPage={currentPage}
            setcurrentPage={setcurrentPage}
            
          />
        </div>
      </Container>
    </>
  );
};

export default Users;
