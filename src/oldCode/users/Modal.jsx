import React, { useState } from "react";
import styled from "styled-components";
import { IoMdClose } from "react-icons/io";
import { useForm } from "react-hook-form";
import ModalFormInput from "../../components/common/ModalFormInput";
import Button from "../../components/common/Button";

const Modal = ({ showModal, content }) => {
  const { register } = useForm();
  const [isUpdating, toggleIsUpdating] = useState(false);
  
  function handleIsUpdating() {
    toggleIsUpdating((prev) => !prev);
  }

  return (
    <ModalLayer className="centralize">
      <div className="card">
        <div className="card_title">
          <h1>{!isUpdating ? "Create User" : "Update information"}</h1>
          <IoMdClose onClick={showModal} className="icon" />
        </div>
        <p>Please enter full information bellow</p>
        {/* card form */}

        <form className="form_card">
          <ModalFormInput
            label="Full name"
            register={register}
            name="fullname"
            type="text"
            placeholder="e.g bobby crooz"
          />

          <ModalFormInput
            label="Email Address"
            register={register}
            name="address"
            type="email"
            placeholder="example@gmail.com"
          />

          <ModalFormInput
            label="Phone Number"
            register={register}
            name="number"
            type="text"
            placeholder="09023456789"
          />

          {!isUpdating && (
            <ModalFormInput
              label="Password"
              register={register}
              name="password"
              type="password"
            />
          )}

          <div className="button_cont">
            <Button height="40px" width="129px" text={ !isUpdating ? "Create New" : "Save"} active />
            <h3 className="centralize" onClick={handleIsUpdating}>Cancle</h3>
            {/* <Button height="40px" width="129px" text="Cancel" /> */}
          </div>
        </form>
      </div>
    </ModalLayer>
  );
};

const ModalLayer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 4;
  width: 100%;
  height: 100%;
  background: #031021a9;

  .card {
    width: 400px;
    /* height: 553px; */
    background: #fff;
    border-radius: 15px;
    /* z-index: 5; */

    p {
      text-align: center;
      font-size: 15px;

    }

    &_title {
      width: 100%;
      height: 50px;
      border-bottom: 1px solid #EFF4FA;
      display: flex;
      justify-content: space-between;
      align-items: flex-end;
      margin-top: 1.5rem;
    }

    h1 {
      /* margin: 5px; */
      margin-left: 20px;
      margin-bottom: 5px;
    }

    .icon {
      /* border: 1px solid red; */
      margin-right: 20px;
      margin-bottom: 6px;
    }
  }
  /* form card */
  .form_card {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin:2rem 0;


    .button_cont {
      width: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      margin-top: 9px;
      h3{
          width:129px;
          height:40px;
          cursor: pointer;
      }
    }
  }

  .button_cont {
    width: 100%;
    display: flex;
    justify-content: center;
    margin-top: 9px;

    
  }
`;

// const Card = styled.div``;

export default Modal;
