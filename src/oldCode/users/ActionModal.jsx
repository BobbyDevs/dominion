import React from "react";
import styled from "styled-components";
import { IoMdClose } from "react-icons/io";

const ActionModal = ({ toggle, item, assigning }) => {
  return (
    <ModalLayer className="centralize">
      <div className="card">
        <div className="card_title border-b h-24 p-12 pb-3  font-black  text-txtColor flex justify-between items-center">
          {!assigning ? (
            <h1>{item !== 1 ? "Activate User" : "Deactivate User"}</h1>
          ) : (
            <h1>Assign Role</h1>
          )}
          <div className="icon cursor-pointer " onClick={toggle}>
            <IoMdClose className="w-9 h-5 " />
          </div>
        </div>

        <div className="content w-full centralize">
          {!assigning ? (
            <p className="text-txtColor mt-6 text-center">
              Are you sure you want to {item === 1 ? "Deactivate" : "Activate"}
              <br />
              <span>Bobby Crooz?</span>
            </p>
          ) : (
            <div className="w-3/4">
              <p className="role_ass text-txtColor mt-6">
                Select a role you want to assign to <span>Bobby Crooz?</span> in
                the dropdown below
              </p>
              <div className="input_container w-full mt-4">
                <select
                  className="w-3/4 h-10 border rounded-md  font-medium"
                  name=""
                  id=""
                >
                  <option value="">Super Administrator</option>
                  <option value="">Super User</option>
                  <option value="">Super Contract</option>
                  <option value="">The Boss</option>
                </select>
              </div>
            </div>
          )}
        </div>

        <div
          className={
            assigning
              ? "button_container  flex  items-center mt-8"
              : "button_container  flex justify-center items-center mt-8"
          }
        >
          {!assigning ? (
            <div>
              {item === 1 ? (
                <button className="deactivate mb-4 action p-4 rounded-lg h-12 centralize w-44">
                  Deactivate User
                </button>
              ) : (
                <button className="activate mb-4 action p-4 rounded-lg h-12 centralize w-44">
                  Activate User
                </button>
              )}
            </div>
          ) : (
            <button className="activate ml-14 action p-4 rounded-lg h-12 centralize w-44">
              Assign Role
            </button>
          )}

          <h2
            onClick={toggle}
            className="cancel text-gray-900 cursor-pointer p-4"
          >
            Cancel
          </h2>
        </div>
      </div>
    </ModalLayer>
  );
};

const ModalLayer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 4;
  width: 100%;
  height: 100%;
  background: #031021a9;

  .card {
    width: 450px;
    height: 350px;
    background: #fff;
    border-radius: 15px;
    h1 {
      font-size: 20px;
    }

    .deactivate {
      background-color: rgba(204, 0, 0, 0.24);
      font-weight: 500;
      color: red;
    }

    .activate {
      background-color: #4bb543;
      font-weight: 400;
      color: #fff;
    }

    .content {
      select {
        /* background:red;
        -webkit--webkit-appearance:none;
        -moz-appearance: none;
        appearance:unset; */
        font-size: 15px;
        color: #29023d;
        border: 1px solid #dccde5;

        &:focus {
          outline: 1px solid #dccde5;
          border-radius: 5px;
        }
      }

      .role_ass {
        width: 100%;
        line-height: 20px;
        color: #39154b;
        font-weight: 500;
        font-size: 16.7px;
      }
      p {
        width: 280px;
        height: 40;
        color: #39154b;
        font-size: 17px;
        span {
          font-weight: 600;
        }
      }
    }
  }
`;

export default ActionModal;
