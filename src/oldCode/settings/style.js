import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  padding: 2rem;
  /* background: red; */
  background: #fbf9fc;
  margin-bottom: 20px;
  padding: 1rem;

  h1 {
    margin-left: 3rem;
    height: 50px;
  }

  .form_container {
    width: 100%;
    height: 100%;

    .card {
      border: 1px solid #edf2f6;
      background: #fff;
      width: 650px;
      height: 520px;
      margin: 3rem;
      padding: 2px 1rem;
      border-radius: 12px;
      margin-left: 5rem;

      .from_title h3 {
        border-bottom: 1px solid #edf2f6;
        padding: 8px;
        width: 60%;
        height: 60px;
        font-weight: 400;
      }

      .form {
        margin-left: 3rem;
        /* form avartar */
        margin-top: 8px;
        .avatar {
          height: 120px;
          h2 {
            margin: 2px;
            margin-top: -8px;
            color: #100119;
            font-size: 20px;
          }
          .photo_action {
            display: flex;
            align-items: center;
            width: 60%;

            .ml {
              margin-left: 1.5rem;
            }

            button {
              background: #dccde5;
              color: #52057b;
              padding: 10px;
              width: 120px;
              border: none;
              border-radius: 8px;
              height: 38px;
            }

            .img {
              width: 70px;
              height: 70px;
            }
          }
        }

        /* form */
        form {
          margin-top: 1rem;

          .button_container {
            display: flex;
            align-items: center;

            h3 {
              width: 129px;
              height: 40px;
              cursor: pointer;
              /* border:1px solid red; */
              margin-top: 25px;
            }
          }

          .input_container {
            height: 180px;
            /* border:1px solid red; */

            width: 470px;
            .form_row {
              display: flex;
              justify-content: space-between;

              .readonly {
                border: 1px solid #dccde5;
                padding: 1rem;
                font-size: 14px;
                width: 220px;
                border-radius: 4px;
                height: 100%;
                margin-top: 10px;
                color: #b9c2c9;
              }

              &.two {
                margin-top: 3rem;
              }
            }
          }
        }
      }
    }
  }
`;
