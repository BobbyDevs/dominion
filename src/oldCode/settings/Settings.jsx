import React, { useContext } from "react";
import Button from "../../components/common/Button";
// import { Heading, Input } from "@chakra-ui/react";
import FormInput from "../../components/FormInput";
import { useForm } from "react-hook-form";
import Ava from "../../assets/ava.png";
import { userContext } from "../../context/userContext";
import { Container } from "./style";

const Settings = () => {
  const { register } = useForm();
  const { User } = useContext(userContext);

  return (
    <Container>
      <h1 className="font-medium text-3xl text-txtColor">Profile settings</h1>

      <div className="form_container">
        <div className="card">
          <div className="from_title mb-0 p-3 py-5 flex  font-medium text-txtColor text-2xl">
            <h3>Edit Profile</h3>
          </div>

          <div className="form">
            <div className="avatar">
              <h2 className="font-bold text-txtColor">Avatar</h2>
              <div className="photo_action mt-4">
                <div className="img ">
                  <img src={Ava} alt="" />
                </div>

                <button className="upload ml font-bold centralize">
                  Upload
                </button>

                <span className="remove ml-2 text-txtColor font-medium">Remove</span>
              </div>
            </div>

            <form action="">
              <div className="input_container">
                <div className="form_row">
                  <FormInput
                    label="Full name"
                    register={register}
                    name="fullname"
                    type="text"
                  />

                  <FormInput
                    label="Email Address"
                    register={register}
                    name="fullname"
                    type="text"
                    value={User.email}
                    read
                  />
                </div>

                <div className="form_row two">
                  <FormInput
                    label="Phone number"
                    register={register}
                    name="fullname"
                    type="text"
                  />

                  <FormInput
                    label="Password"
                    register={register}
                    name="fullname"
                    type="text"
                  />
                </div>
              </div>
              <div className="button_container ">
                <Button
                  active
                  width="auto"
                  height="38px"
                  text="save changes"
                  mT="10px"
                  type="submit"
                />

                <h3 className="centralize mb-3">Cancel</h3>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Settings;
